ElfGames-Extensions
======================

A set of utility class to improve the Unity experience.

It includes:

* Property Drawers
  * **SortingLayerName**: showing a select box for sorting layers
  * **MaskList**: lists elements in an array as a select box, storing them as a bitmask
  * **DraggablePoint**: shows handles to drag&drop points/rects/bounds in the editor
  * **HavingInterface**: requires components to inherit from a certain interface
  * **HavingType**: requires game objects to have a certain component/interface
* Components
  * **PolygonMesh2D**: a in-editor polygon mesh builder
  * **RendererSortingLayer**: exposing non-sprite renderers sorting layer & order. Requires a `Transparent`-queued shader to work.
* Helpers for **Singletons**, **Persistent Singletons** and **Global scripts**
* **WaitForEvent** a pattern for waiting `UnityEvent`s inside Coroutines
* A few Module Extensions, for Math (DivMod), MonoBehaviors (Invoke, InvokeRepeating), and Coroutines (with error handling and output values)
* UnityEngine.UI extensions
  * **MaskWithCutoff**: a Mask with an alpha cutoff parameter
* A few general C# utility classes (Params, Interpolator, Serializable objects)
