﻿using System.Collections;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class ExtensionsTest
    {
        // A Test behaves as an ordinary method
        [Test]
        public void ToFloatPasses()
        {
            var bigNumber = 8532.325f;
            var stringNumber = string.Format("{0}", bigNumber);
            Assert.AreEqual(stringNumber, "8532.325");
            Assert.AreEqual(bigNumber, stringNumber.ToFloat());
        }

        [Test]
        public void AsVector2Passes()
        {
            var stringVector = string.Format("{0},{1}", 10.5f, 20.3f);
            Assert.AreEqual(new Vector2(10.5f, 20.3f), stringVector.AsVector2());
        }

        [Test]
        public void AsVector3Passes()
        {
            var stringVector = string.Format("{0},{1},{2}", 10.5f, 20.3f, 11.3f);
            Assert.AreEqual(new Vector3(10.5f, 20.3f, 11.3f), stringVector.AsVector3());
        }

        [Test]
        public void AsColorPasses()
        {
            var stringVector = string.Format("{0},{1},{2},{3}", 0.5f, 0.32f, 0.2236665f, 0.2231f);
            Assert.AreEqual(new Color(0.5f, 0.32f, 0.2236665f, 0.2231f), stringVector.AsColor());
        }
    }
}
