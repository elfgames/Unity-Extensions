using System;
using UnityEngine;

namespace ElfGames.Extensions
{
    public static class ShapeDrawer
    {
        public static void DrawCircle(Vector2 center, float radius, Action<Vector2> drawingFunction)
        {
            for (int i = 0; i < 360; i++)
            {
                var angle = i * Mathf.Deg2Rad;
                var x = center.x + Mathf.Cos(angle) * radius;
                var y = center.y + Mathf.Sin(angle) * radius;
                drawingFunction(new Vector2(x, y));
            }
        }

        public static void DrawRectangle(Vector2 center, Vector2 size, Action<Vector2> drawingFunction)
        {
            var halfSize = size / 2;
            var topLeft = center - halfSize;
            var topRight = center + new Vector2(halfSize.x, -halfSize.y);
            var bottomRight = center + halfSize;
            var bottomLeft = center + new Vector2(-halfSize.x, halfSize.y);

            drawingFunction(topLeft);
            drawingFunction(topRight);
            drawingFunction(bottomRight);
            drawingFunction(bottomLeft);
        }

        public static void DrawLine(Vector2 start, Vector2 end, Action<Vector2> drawingFunction)
        {
            var direction = (end - start).normalized;
            var distance = (end - start).magnitude;
            for (int i = 0; i < distance; i++)
            {
                var point = start + direction * i;
                drawingFunction(point);
            }
        }

        public static void DrawArrow(Vector2 start, Vector2 end, float arrowSize, Action<Vector2> drawingFunction)
        {
            var direction = (end - start).normalized;
            var perpendicular = new Vector2(-direction.y, direction.x);
            var arrowEnd = end - direction * arrowSize;
            var arrowLeft = arrowEnd + perpendicular * arrowSize;
            var arrowRight = arrowEnd - perpendicular * arrowSize;

            DrawLine(start, end, drawingFunction);
            DrawLine(end, arrowLeft, drawingFunction);
            DrawLine(end, arrowRight, drawingFunction);
        }
    }
}
