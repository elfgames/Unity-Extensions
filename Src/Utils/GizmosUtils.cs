using UnityEngine;

public static class GizmosUtils
{
    public static void DrawSquare(Vector2 center, Vector2 size)
    {
        var halfSize = size / 2;

        Gizmos.DrawLine(center + Vector2.up * halfSize.y + Vector2.left * halfSize.x, center + Vector2.up * halfSize.y + Vector2.right * halfSize.x);
        Gizmos.DrawLine(center + Vector2.down * halfSize.y + Vector2.left * halfSize.x, center + Vector2.down * halfSize.y + Vector2.right * halfSize.x);

        Gizmos.DrawLine(center + Vector2.up * halfSize.y + Vector2.right * halfSize.x, center + Vector2.down * halfSize.y + Vector2.right * halfSize.x);
        Gizmos.DrawLine(center + Vector2.up * halfSize.y + Vector2.left * halfSize.x, center + Vector2.down * halfSize.y + Vector2.left * halfSize.x);
    }

    public static void DrawRect(Rect rect, float z = 0)
    {
        Vector3 bl = rect.min;
        Vector3 br = rect.min + rect.width * Vector2.right;
        Vector3 tl = rect.max + rect.width * Vector2.left;
        Vector3 tr = rect.max;
        bl.z = br.z = tl.z = tr.z = z;
        DrawRect(bl, br, tl, tr);
    }

    public static void DrawRect(Vector3 bl, Vector3 br, Vector3 tl, Vector3 tr)
    {
        Gizmos.DrawLine(bl, br);
        Gizmos.DrawLine(bl, tl);
        Gizmos.DrawLine(tl, tr);
        Gizmos.DrawLine(tr, br);
    }

    public static void DrawCircle(Vector3 center, float radius, float precision = 0.01f)
    {
        for (var r = 0f; r < 2 * Mathf.PI; r += Mathf.PI * precision)
        {
            var nextStep = r + Mathf.PI * precision;
            var p1 = new Vector2(center.x + Mathf.Cos(r) * radius, center.y + Mathf.Sin(r) * radius);
            var p2 = new Vector2(center.x + Mathf.Cos(nextStep) * radius, center.y + Mathf.Sin(nextStep) * radius);
            Gizmos.DrawLine(p1, p2);
        }
    }

    public static void DrawLinked3DRects(Rect rect1, Rect rect2, float z1, float z2)
    {
        Vector3 bl1 = rect1.min;
        Vector3 br1 = rect1.min + rect1.width * Vector2.right;
        Vector3 tl1 = rect1.max + rect1.width * Vector2.left;
        Vector3 tr1 = rect1.max;
        bl1.z = br1.z = tl1.z = tr1.z = z1;

        Vector3 bl2 = rect2.min;
        Vector3 br2 = rect2.min + rect2.width * Vector2.right;
        Vector3 tl2 = rect2.max + rect2.width * Vector2.left;
        Vector3 tr2 = rect2.max;
        bl2.z = br2.z = tl2.z = tr2.z = z2;

        DrawRect(bl1, br1, tl1, tr1);
        DrawRect(bl2, br2, tl2, tr2);

        Gizmos.DrawLine(bl1, bl2);
        Gizmos.DrawLine(br1, br2);
        Gizmos.DrawLine(tl1, tl2);
        Gizmos.DrawLine(tr1, tr2);
    }
}
