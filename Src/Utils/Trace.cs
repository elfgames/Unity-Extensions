﻿using System;
using System.Diagnostics;

namespace ElfGames.Extensions
{
    public static class Trace
    {
        [Conditional("UNITY_DEBUG")]
        public static void Log(string msg, params object[] data)
        {
            UnityEngine.Debug.Log(string.Format(msg, data));
        }

        [Conditional("UNITY_DEBUG")]
        public static void Log(object msg)
        {
            UnityEngine.Debug.Log(msg);
        }

        [Conditional("UNITY_DEBUG")]
        public static void LogWarning(object msg)
        {
            UnityEngine.Debug.LogWarning(msg);
        }

        [Conditional("UNITY_DEBUG")]
        public static void LogWarning(string format, params object[] data)
        {
            UnityEngine.Debug.LogWarningFormat(format, data);
        }

        [Conditional("UNITY_DEBUG")]
        public static void LogError(object msg)
        {
            UnityEngine.Debug.LogError(msg);
        }

        [Conditional("UNITY_DEBUG")]
        public static void LogError(string format, params object[] data)
        {
            UnityEngine.Debug.LogErrorFormat(format, data);
        }
    }
}
