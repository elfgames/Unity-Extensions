﻿using System.Collections;
using UnityEngine;
using ElfGames.Extensions.Easing;

namespace ElfGames.Extensions
{
	public static class Interpolator
	{

		public static IEnumerator Run(float from, float to, System.Action<float> setter, float time, EaseDelegate easing = null)
		{
			var t = 0f;
			while (t < time)
			{
				yield return 0;
				t += Time.deltaTime;
				if (easing == null)
				{
					setter.Invoke(Mathf.Lerp(from, to, t / time));
				}
				else
				{
					setter.Invoke(from + ((to - from) * (float)easing(t / time, 0, 1, 1)));
				}
			}
			setter.Invoke(to);
		}

		public static IEnumerator Run(Vector2 from, Vector2 to, System.Action<Vector2> setter, float time, EaseDelegate easing = null)
		{
			var t = 0f;
			while (t < time)
			{
				yield return 0;
				t += Time.deltaTime;
				if (easing == null)
				{
					setter.Invoke(Vector2.Lerp(from, to, t / time));
				}
				else
				{
					setter.Invoke(from + ((to - from) * (float)easing(t / time, 0, 1, 1)));
				}
			}
			setter.Invoke(to);
		}

		public static IEnumerator Run(Vector3 from, Vector3 to, System.Action<Vector3> setter, float time, EaseDelegate easing = null)
		{
			var t = 0f;
			while (t < time)
			{
				yield return 0;
				t += Time.deltaTime;
				if (easing == null)
				{
					setter.Invoke(Vector3.Lerp(from, to, t / time));
				}
				else
				{
					setter.Invoke(from + ((to - from) * (float)easing(t / time, 0, 1, 1)));
				}
			}
			setter.Invoke(to);
		}

		public static IEnumerator Run(Color from, Color to, System.Action<Color> setter, float time, EaseDelegate easing = null)
		{
			var t = 0f;
			while (t < time)
			{
				yield return 0;
				t += Time.deltaTime;
				if (easing == null)
				{
					setter.Invoke(Color.Lerp(from, to, t / time));
				}
				else
				{
					setter.Invoke(from + ((to - from) * (float)easing(t / time, 0, 1, 1)));
				}
			}
			setter.Invoke(to);
		}
	}
}
