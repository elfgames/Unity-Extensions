﻿using System.Collections;
using System.Collections.Generic;

namespace ElfGames.Extensions
{
    public class ObservableList<T> : IList<T>
    {
        public delegate void ObservableListDelegate(T value, int index);
        public event ObservableListDelegate OnInsert;
        public event ObservableListDelegate OnRemove;

        protected IList<T> list;
        public ObservableList(IEnumerable<T> list)
        {
            this.list = new List<T>(list);
            OnInsert = (a, b) => { };
            OnRemove = (a, b) => { };
        }
        public ObservableList(int capacity)
        {
            list = new List<T>(capacity);
            OnInsert = (a, b) => { };
            OnRemove = (a, b) => { };
        }
        public T this[int index]
        {
            get
            {
                return list[index];
            }
            set
            {
                var oldValue = list[index];
                if (!oldValue.Equals(value))
                {
                    list[index] = value;

                    OnRemove(oldValue, index);
                    OnInsert(value, index);
                }
            }
        }

        public int Count
        {
            get
            {
                return list.Count;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return list.IsReadOnly;
            }
        }

        public void Add(T item)
        {
            list.Add(item);
            OnInsert(item, list.Count - 1);
        }

        public void Clear()
        {
            var oldList = new List<T>(list);
            list.Clear();
            for (var i = oldList.Count - 1; i >= 0; i--)
            {
                OnRemove(oldList[i], i);
            }
        }

        public bool Contains(T item)
        {
            return list.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            list.CopyTo(array, arrayIndex);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return list.GetEnumerator();
        }

        public int IndexOf(T item)
        {
            return list.IndexOf(item);
        }

        public void Insert(int index, T item)
        {
            list.Insert(index, item);

            OnInsert(item, index);
        }

        public bool Remove(T item)
        {
            var index = list.IndexOf(item);
            if (index >= 0)
            {
                RemoveAt(index);
                return true;
            }
            return false;
        }

        public void RemoveAt(int index)
        {
            var item = list[index];
            list.RemoveAt(index);
            OnRemove(item, index);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return list.GetEnumerator();
        }
    }
}
