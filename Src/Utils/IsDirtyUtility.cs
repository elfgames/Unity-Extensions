﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Reflection;
using System;
using System.Linq;

namespace ElfGames.Extensions
{

    // Special thanks to Shamanim for solution
    // https://forum.unity.com/threads/sorta-solved-check-if-gameobject-is-dirty.504894/#post-3967810
    public static class IsDirtyUtility
    {
        //Cached Value
        private static Func<int, bool> _isDirtyCallback;

        private static Func<int, bool> IsDirtyCallback
        {
            get
            {
                if (_isDirtyCallback == null)
                {
                    MethodInfo isDirtyMethod = typeof(EditorUtility).GetMethods(BindingFlags.NonPublic | BindingFlags.Static).FirstOrDefault(e => e.Name == "IsDirty" && e.GetParameters()[0].ParameterType == typeof(int));
                    _isDirtyCallback = (Func<int, bool>)Delegate.CreateDelegate(typeof(Func<int, bool>), null, isDirtyMethod);
                }

                return _isDirtyCallback;
            }
        }

        public static bool IsDirty(int instanceID)
        {
            return IsDirtyCallback(instanceID);
        }

        public static bool IsDirty(GameObject gameObject)
        {
            return IsDirtyCallback(gameObject.GetInstanceID());
        }

    }
}
#endif
