﻿using System.Linq;
using UnityEngine;

namespace ElfGames.Extensions
{
    /// <summary>
    ///     This script keeps assigned the camera to the canvas worldCamera.
    /// </summary>
    [ExecuteAlways]
    [RequireComponent(typeof(Canvas))]
    public class AssignCameraToCanvas : MonoBehaviour
    {
        [SerializeField] protected bool useMainCamera = true;
        [ShowConditional("useMainCamera", false)]
        [SerializeField] protected string cameraName;
        private Canvas _canvas;
        private Camera _current;

        public Camera Current
        {
            get
            {
                if (_current == null)
                {
                    _current = useMainCamera ? Camera.main : Camera.allCameras.FirstOrDefault(c => c.gameObject.name == cameraName);
                }
                return _current;
            }
        }

        /// <summary>
        ///     All'awake assegno il Canvas dell'oggetto a cui è assegnato questo script
        /// </summary>
        protected void Awake()
        {
            _canvas = GetComponent<Canvas>();
        }

        private void OnDisable()
        {
            _current = null;
        }

        /// <summary>
        ///     Continuo ad assegnare la camera corretta in modo che funzioni anche al cambio di scena
        /// </summary>
        protected void Update()
        {
            _canvas.worldCamera = Current;
        }
    }
}
