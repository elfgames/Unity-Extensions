﻿using System.Collections.Generic;
using UnityEngine;

namespace ElfGames.Extensions
{
    // <summary>
    //    A class that allows you to pass a dictionary of parameters to a function.
    //    It also allows you to specify default values for the parameters.
    //    It is useful for functions that have a lot of parameters.
    // </summary>
    public class Params : Dictionary<string, object>
    {
        public static Params Put(params object[] elements)
        {
            var p = new Params();
            for (var i = 0; i < elements.Length; i += 2)
            {
                try
                {
                    p[(string)elements[i]] = elements[i + 1];
                }
                catch (System.InvalidCastException e)
                {
                    Debug.LogErrorFormat("There was an error while casting {0} to {1}", elements[i].GetType(), typeof(string));
                    throw e;
                }
            }
            return p;
        }

        public bool Has(string key)
        {
            return ContainsKey(key);
        }

        public T Get<T>(string key)
        {
            try
            {
                return ContainsKey(key) ? (T)this[key] : default(T);
            }
            catch (System.InvalidCastException e)
            {
                Debug.LogErrorFormat("There was an error while casting {0} to {1}. Actual value: {2}", key, typeof(T), this[key]);
                throw e;
            }
        }

        public static Params Merge(params Params[] items)
        {
            var p = new Params();
            foreach (var item in items)
            {
                foreach (var kv in item)
                {
                    p[kv.Key] = kv.Value;
                }
            }
            return p;
        }

        public static Params Default(Params element, params object[] elements)
        {
            element = element ?? new Params();
            for (var i = 0; i < elements.Length; i += 2)
            {
                var key = (string)elements[i];
                if (!element.ContainsKey(key))
                {
                    element[key] = elements[i + 1];
                }
            }
            return element;
        }

#if UNITY_EDITOR
        public override string ToString()
        {
            var output = new System.Text.StringBuilder();
            output.Append("{\n");
            foreach (var kv in this)
            {
                output.Append($"\t{kv.Key} => {kv.Value},\n");
            }
            output.Append("}");
            return output.ToString();
        }
#endif
    }
}
