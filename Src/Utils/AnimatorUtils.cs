﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor.Animations;
using System.Linq;
using System;
using UnityEditor;

namespace ElfGames.Extensions.Editor
{

    public static class AnimatorUtils
    {
        public static AnimatorController GetAnimatorController(Component component)
        {
            if (component == null)
            {
                return null;
            }
            return GetAnimatorController(component.gameObject);
        }

        public static AnimatorController GetAnimatorController(GameObject gameObject)
        {
            var animator = FindAnimator(gameObject);
            if (animator == null)
            {
                return null;
            }
            return animator.runtimeAnimatorController as AnimatorController;
        }

        public static Animator FindAnimator(Component component)
        {
            if (component == null)
            {
                return null;
            }
            return FindAnimator(component.gameObject);
        }

        public static Animator FindAnimator(GameObject gameObject)
        {
            if (gameObject == null)
            {
                return null;
            }
            return gameObject.GetComponentInChildren<Animator>();
        }

        public static string[] GetParameters(AnimatorController controller)
        {
            if (controller == null)
            {
                return Array.Empty<string>();
            }

            return controller.parameters.Select(e => e.name).ToArray();
        }

        public static string[] GetAnimations(GameObject gameObject)
        {
            var animator = FindAnimator(gameObject);
            if (animator == null)
            {
                return null;
            }
            return GetAnimations(animator);
        }

        public static string[] GetAnimations(Animator animator)
        {
            var animatorController = animator.runtimeAnimatorController as AnimatorController;
            return GetAnimations(animatorController);
        }

        public static string[] GetAnimations(AnimatorController animatorController)
        {
            return animatorController.layers.SelectMany(e => e.stateMachine.states).Select(e => e.state.name).OrderBy(e => e).ToArray();
        }

       public static AnimatorController FindSomeKindOfAnimatorController(SerializedProperty property)
        {
            var component = property.serializedObject.targetObject as Component;
            var animatorController = AnimatorUtils.GetAnimatorController(component);

            if (animatorController == null) {
                animatorController = GetAnimatorWithReflection(property.serializedObject);
            }

            if (animatorController == null) {
                animatorController = AnimatorUtils.GetAnimatorController(Selection.activeGameObject);
            }
            return animatorController;
        }

        public static AnimatorController GetAnimatorWithReflection(SerializedObject serializedObject)
        {
            var target = serializedObject.targetObject;
            var method = target.GetType().GetProperty("animator", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance);
            return (method?.GetValue(target, null) as Animator)?.runtimeAnimatorController as AnimatorController;
        }
    }

}
#endif
