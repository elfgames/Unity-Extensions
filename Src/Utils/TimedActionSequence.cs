﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace ElfGames.Extensions
{
    // <summary>
    //     A class that allows you to run a sequence of actions in a coroutine.
    //     The actions can be any of the following:
    //     - IEnumerator
    //     - YieldInstruction
    //     - UnityEvent
    //     - System.Action
    //     - Any other object that will be yielded
    // </summary>
    public static class TimedActionSequence
    {
        public static Coroutine Start(MonoBehaviour caller, params object[] actions)
        {
            return caller.StartCoroutine(Run(caller, actions));
        }

        static IEnumerator Run(MonoBehaviour caller, object[] actions)
        {
            foreach (var action in actions)
            {
                if (action is IEnumerator)
                {
                    yield return caller.StartCoroutine(action as IEnumerator);
                }
                else if (action is YieldInstruction)
                {
                    yield return action;
                }
                else if (action is UnityEvent)
                {
                    (action as UnityEvent).Invoke();
                }
                else if (action is System.Action)
                {
                    (action as System.Action).Invoke();
                }
                else
                {
                    yield return action;
                }
            }
        }
    }
}
