﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace ElfGames.Extensions
{
    /// <summary>
    /// Object pool for Unity
    /// </summary>
    public class ObjectPool<T> : System.IDisposable where T : Component
    {
        readonly HashSet<T> used;
        readonly Queue<T> unused;

        public T Prefab { get; private set; }
        public Transform Parent { get; private set; }

        public int Count => used.Count;

        private readonly System.Action<T> onAllocate;
        private readonly System.Action<T> onFree;

        public ObjectPool(int capacity, T prefab, Transform parent, System.Action<T> onAllocate = null, System.Action<T> onFree = null)
        {
            Assert.IsNotNull(prefab, "Prefab is null");
            Assert.IsTrue(capacity > 0, "Capacity is less than 1");

            used = new HashSet<T>();
            unused = new Queue<T>(capacity);

            Prefab = prefab;
            Parent = parent;

            this.onAllocate = onAllocate;
            this.onFree = onFree;

            for (var i = 0; i < capacity; i++)
            {
                var element = Object.Instantiate(prefab, parent);
                element.gameObject.SetActive(false);
                unused.Enqueue(element);
            }
        }


        public T Allocate()
        {
            T newRect;
            if (unused.Count == 0)
            {
                newRect = Object.Instantiate(Prefab, Parent);
            }
            else
            {
                newRect = unused.Dequeue();
            }
            newRect.gameObject.SetActive(true);
            used.Add(newRect);
            newRect.SendMessage("OnPoolActivate", -1, SendMessageOptions.DontRequireReceiver);
            onAllocate?.Invoke(newRect);
            return newRect;
        }

        public bool Free(T item)
        {
            if (!used.Remove(item))
            {
                return false;
            }
            unused.Enqueue(item);
            item.SendMessage("OnPoolDeactivate", -1, SendMessageOptions.DontRequireReceiver);
            item.gameObject.SetActive(false);
            onFree?.Invoke(item);
            return true;
        }

        public void Clear()
        {
            foreach (var item in used)
            {
                if (item == null)
                {
                    continue;
                }
                item.SendMessage("OnPoolDeactivate", -1, SendMessageOptions.DontRequireReceiver);
                item.gameObject.SetActive(false);
                onFree?.Invoke(item);
                unused.Enqueue(item);
            }
            used.Clear();
        }

        public void Dispose()
        {
            Clear();
            while (unused.Count > 0)
            {
                var item = unused.Dequeue();
                if (item != null)
                {
                    Object.Destroy(item.gameObject);
                }
            }
        }
    }
}

