﻿using System.Collections;
using System.Collections.Generic;

namespace ElfGames.Extensions
{
    public class ObservableDictionary<TKey, TValue> : IDictionary<TKey, TValue>
    {
        public delegate void ObservableDictionaryDelegate(TKey key, TValue value);
        public delegate void ObservableDictionaryChangeDelegate(TKey key, TValue value, TValue oldValue);
        public event ObservableDictionaryDelegate OnInsert;
        public event ObservableDictionaryChangeDelegate OnChange;
        public event ObservableDictionaryDelegate OnRemove;

        protected IDictionary<TKey, TValue> dictionary;

        public ICollection<TKey> Keys
        {
            get
            {
                return dictionary.Keys;
            }
        }

        public ICollection<TValue> Values
        {
            get
            {
                return dictionary.Values;
            }
        }

        public int Count
        {
            get
            {
                return dictionary.Count;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return dictionary.IsReadOnly;
            }
        }

        public ObservableDictionary(IDictionary<TKey, TValue> dictionary)
        {
            this.dictionary = dictionary;
            OnInsert = (a, b) => { };
            OnChange = (a, b, c) => { };
            OnRemove = (a, b) => { };
        }
        public ObservableDictionary(int capacity) : this(new Dictionary<TKey, TValue>(capacity))
        {
        }

        public TValue this[TKey key]
        {
            get
            {
                return dictionary[key];
            }
            set
            {
                if (ContainsKey(key))
                {
                    var oldValue = dictionary[key];
                    dictionary[key] = value;
                    if (!oldValue.Equals(value))
                    {
                        OnChange(key, value, oldValue);
                    }
                }
                else
                {
                    dictionary[key] = value;

                    OnInsert(key, value);
                }
            }
        }

        public void Add(TKey key, TValue value)
        {
            dictionary.Add(key, value);
            OnInsert(key, value);
        }

        public bool ContainsKey(TKey key)
        {
            return dictionary.ContainsKey(key);
        }

        public bool Remove(TKey key)
        {
            if (!ContainsKey(key))
            {
                return false;
            }
            var value = dictionary[key];
            dictionary.Remove(key);
            OnRemove(key, value);
            return true;
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            return dictionary.TryGetValue(key, out value);
        }

        public void Add(KeyValuePair<TKey, TValue> item)
        {
            dictionary.Add(item);
            OnInsert(item.Key, item.Value);
        }

        public void Clear()
        {
            var oldDictionary = new Dictionary<TKey, TValue>(dictionary);
            dictionary.Clear();
            foreach (var keyVal in oldDictionary)
            {
                OnRemove(keyVal.Key, keyVal.Value);
            }
        }

        public bool Contains(KeyValuePair<TKey, TValue> item)
        {
            return dictionary.Contains(item);
        }

        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            dictionary.CopyTo(array, arrayIndex);
        }

        public bool Remove(KeyValuePair<TKey, TValue> item)
        {
            if (!Contains(item))
            {
                return false;
            }
            dictionary.Remove(item);
            OnRemove(item.Key, item.Value);
            return true;
        }

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return dictionary.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return dictionary.GetEnumerator();
        }
    }
}
