using UnityEngine;

namespace ElfGames.Extensions
{
    /// <summary>
    /// A property drawer that displays a popup with the list of tags.
    /// </summary>
    [System.AttributeUsage(System.AttributeTargets.Field, Inherited = false, AllowMultiple = false)]
    public sealed class TagSelector : PropertyAttribute
    {
    }


#if UNITY_EDITOR
    // The editor for the property tag selector
    [UnityEditor.CustomPropertyDrawer(typeof(TagSelector))]
    public class TagSelectorDrawer : UnityEditor.PropertyDrawer
    {
        public override void OnGUI(Rect position, UnityEditor.SerializedProperty property, GUIContent label)
        {
            if (property.propertyType == UnityEditor.SerializedPropertyType.String)
            {
                var tags = UnityEditorInternal.InternalEditorUtility.tags;
                var selectedIndex = 0;
                for (var i = 0; i < tags.Length; i++)
                {
                    if (tags[i] == property.stringValue)
                    {
                        selectedIndex = i;
                        break;
                    }
                }
                selectedIndex = UnityEditor.EditorGUI.Popup(position, label.text, selectedIndex, tags);
                property.stringValue = tags[selectedIndex];
            }
            else
            {
                UnityEditor.EditorGUI.PropertyField(position, property, label);
            }
        }
    }
#endif
}
