﻿using System;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace ElfGames.Extensions
{

    /// <summary>
    /// A property drawer that checks if the assigned object has the specified components.
    /// </summary>
    public class HavingComponents : PropertyAttribute
    {
        public Type[] Types { get; private set; }

        public HavingComponents(params Type[] types)
        {
            Types = types;
        }
    }


    #if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(HavingComponents))]
    public class HavingComponentsDrawer : PropertyDrawer
    {

        public GUIStyle Errors = new GUIStyle();

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            if (FirstInvalidType(property.objectReferenceValue) != null)
            {
                return base.GetPropertyHeight(property, label) * 2;
            }
            return base.GetPropertyHeight(property, label);
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var reference = property.objectReferenceValue;
            var invalidType = FirstInvalidType(reference);

            if (invalidType != null)
            {
                var rect = position;
                rect.height = base.GetPropertyHeight(property, label);

                var savedColor = EditorStyles.objectField.normal.textColor;
                EditorStyles.objectField.normal.textColor = Color.red;
                EditorGUI.PropertyField(rect, property);
                EditorStyles.objectField.normal.textColor = savedColor;

                Errors.normal.textColor = Color.red;
                Errors.alignment = TextAnchor.MiddleRight;
                rect.y += rect.height;
                if (reference == null)
                {
                    EditorGUI.LabelField(rect, "Reference not assigned.", Errors);
                }
                else
                {
                    EditorGUI.LabelField(rect, "Missing component {0}.".FormatWith(invalidType.Name), Errors);
                }

                return;
            }
            EditorGUI.PropertyField(position, property);
        }

        Type FirstInvalidType(UnityEngine.Object obj)
        {
            if (obj == null)
            {
                return typeof(UnityEngine.Object);
            }
            var types = ((HavingComponents)attribute).Types;
            foreach (var type in types)
            {
                if (!HasComponent(obj, type))
                {
                    return type;
                }
            }
            return null;
        }

        bool HasComponent(UnityEngine.Object obj, Type type)
        {
            return (obj as GameObject)?.GetComponent(type) != null ||
                (obj as Component)?.GetComponent(type) != null ||
                (obj as Transform)?.GetComponent(type) != null;
        }
    }
    #endif
}
