﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace ElfGames.Extensions
{

    /// <summary>
    /// A property drawer that displays a mask selector with a list of strings and saves the result as an integer.
    /// </summary>
    public class MaskList : PropertyAttribute
    {
        public string[] Words
        {
            get;
            private set;
        }
        public MaskList(params string[] words)
        {
            Words = words;
        }
    }

#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(MaskList))]
    public class MaskListDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var words = (attribute as MaskList).Words;
            if (property.propertyType == SerializedPropertyType.Integer)
            {
                EditorGUI.BeginChangeCheck();
                property.intValue = EditorGUI.MaskField(position, property.intValue, words);
                if (EditorGUI.EndChangeCheck())
                {
                    property.serializedObject.ApplyModifiedProperties();
                }
            }
        }
    }
#endif
}
