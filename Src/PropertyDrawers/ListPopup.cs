﻿using System;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace ElfGames.Extensions
{
    /// <summary>
    /// A property drawer that displays a popup with a list of strings.
    /// </summary>
    public class ListPopup : PropertyAttribute
    {
        public delegate string[] GetStringList();

        public ListPopup(params string[] list)
        {
            List = list;
        }

        public string[] List
        {
            get;
            private set;
        }
    }


    #if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(ListPopup))]
    public class ListPopupDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var stringInList = attribute as ListPopup;
            var list = stringInList.List;
            if (property.propertyType == SerializedPropertyType.String)
            {
                int index = Mathf.Max(0, Array.IndexOf(list, property.stringValue));
                index = EditorGUI.Popup(position, property.displayName, index, list);

                property.stringValue = list[index];
            }
            else if (property.propertyType == SerializedPropertyType.Integer)
            {
                property.intValue = EditorGUI.Popup(position, property.displayName, property.intValue, list);
            }
            else
            {
                base.OnGUI(position, property, label);
            }
        }
    }
    #endif
}
