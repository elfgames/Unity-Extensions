﻿using System;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace ElfGames.Extensions
{

    /// <summary>
    /// A property drawer that checks that the assigned value is not null.
    /// </summary>
    public class RequiredAttribute : PropertyAttribute
    {
        public string Message { get; private set; }
        public RequiredAttribute(string message = "$name$ is <b>required</b>.")
        {
            Message = message;
        }
    }


#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(RequiredAttribute))]
    public class RequiredAttributeDrawer : PropertyDrawer
    {

        public GUIStyle Errors = new GUIStyle();

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            if (IsNull(property))
            {
                return base.GetPropertyHeight(property, label) * 2;
            }
            else
            {
                return base.GetPropertyHeight(property, label);
            }
        }

        // Draw the property inside the given rect
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (IsNull(property))
            {
                Errors.normal.textColor = Color.red;
                Errors.alignment = TextAnchor.MiddleRight;
                Errors.richText = true;
                var message = (attribute as RequiredAttribute).Message.Replace("$name$", property.displayName);
                label.tooltip = message;

                var savedColor = EditorStyles.objectField.normal.textColor;
                try
                {
                    EditorStyles.objectField.normal.textColor = Color.red;
                    EditorGUI.ObjectField(new Rect(position.x, position.y, position.width, position.height * 0.5f), property, label);
                }
                finally
                {
                    EditorStyles.objectField.normal.textColor = savedColor;
                }

                EditorGUI.LabelField(new Rect(position.x, position.y + position.height * 0.5f, position.width, position.height * 0.5f), message, Errors);
                if (Application.isPlaying)
                {
                    Debug.LogError(message, property.serializedObject.context);
                }
            }
            else
            {
                EditorGUI.PropertyField(position, property, label, true);
            }
        }

        public bool IsNull(SerializedProperty property)
        {
            return property.propertyType == SerializedPropertyType.ObjectReference && property.objectReferenceValue == null;
        }
    }
#endif
}
