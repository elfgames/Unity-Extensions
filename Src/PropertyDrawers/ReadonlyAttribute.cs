﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace ElfGames.Extensions
{
    /// <summary>
    /// A property drawer that makes a field readonly.
    /// </summary>
    public class ReadonlyAttribute : PropertyAttribute { }

    #if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(ReadonlyAttribute))]
    public class ReadonlyAttributeDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var labelRect = new Rect(position);
            labelRect.width = EditorGUIUtility.labelWidth;
            var contentRect = new Rect(position);
            contentRect.x += EditorGUIUtility.labelWidth;
            contentRect.width -= EditorGUIUtility.labelWidth;

            EditorGUI.LabelField(labelRect, label);
            switch (property.propertyType)
            {
                case SerializedPropertyType.Boolean:
                    EditorGUI.LabelField(contentRect, property.boolValue.ToString());
                    break;
                case SerializedPropertyType.Float:
                    EditorGUI.LabelField(contentRect, property.floatValue.ToString());
                    break;
                case SerializedPropertyType.Color:
                    EditorGUI.LabelField(contentRect, property.colorValue.ToString());
                    break;
                case SerializedPropertyType.Integer:
                    EditorGUI.LabelField(contentRect, property.intValue.ToString());
                    break;
                case SerializedPropertyType.Vector2:
                    EditorGUI.LabelField(contentRect, property.vector2Value.ToString());
                    break;
                case SerializedPropertyType.Vector3:
                    EditorGUI.LabelField(contentRect, property.vector3Value.ToString());
                    break;
                case SerializedPropertyType.Enum:
                    EditorGUI.LabelField(contentRect, property.enumDisplayNames[property.enumValueIndex]);
                    break;
                case SerializedPropertyType.String:
                    EditorGUI.LabelField(contentRect, property.stringValue);
                    break;
                case SerializedPropertyType.Bounds:
                    EditorGUI.LabelField(contentRect, property.boundsValue.ToString());
                    break;
                default:
                    base.OnGUI(position, property, label);
                    break;
            }
        }
    }
    #endif
}
