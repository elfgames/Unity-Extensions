﻿using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace ElfGames.Extensions
{
    /// <summary>
    ///     Attribute to add a button to a string field in the inspector.
    ///     The string field should be the name of a method in the same class.
    ///     The method should take a single string parameter.
    /// </summary>
    public class ActionButtonAttribute : PropertyAttribute
    {
        public string Label { get; private set; }
        public ActionButtonAttribute(string label)
        {
            Label = label;
        }
    }

#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(ActionButtonAttribute))]
    public class ActionButtonDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (property.propertyType != SerializedPropertyType.String)
            {
                EditorGUI.LabelField(position, "Invalid attribute type");
                return;
            }

            var type = property.serializedObject.targetObject.GetType();
            var methodName = property.name + "Action";

            var method = type.GetMethod(methodName, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.DeclaredOnly | BindingFlags.Public);

            if (method == null)
            {
                EditorGUI.LabelField(position, "No such method {0} in {1}".FormatWith(methodName, type));
                return;
            }

            if (GUI.Button(position, (attribute as ActionButtonAttribute).Label))
            {
                method.Invoke(property.serializedObject.targetObject, new[] { property });
            }
        }
    }
#endif
}

