﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace ElfGames.Extensions
{

    /// <summary>
    ///     A property drawer that shows a field only if the specified attribute is true.
    /// </summary>
    public class ShowConditionalAttribute : PropertyAttribute
    {
        public string AttributeName { get; private set; }
        public object AttributeValue { get; private set; }

        public ShowConditionalAttribute(string attributeName, bool value)
        {
            AttributeName = attributeName;
            AttributeValue = value;
        }

        public ShowConditionalAttribute(string attributeName, int value)
        {
            AttributeName = attributeName;
            AttributeValue = value;
        }

        public ShowConditionalAttribute(string attributeName, float value)
        {
            AttributeName = attributeName;
            AttributeValue = value;
        }

        public ShowConditionalAttribute(string attributeName, object value)
        {
            AttributeName = attributeName;
            AttributeValue = value;
        }

        public ShowConditionalAttribute(string attributeName, string value)
        {
            AttributeName = attributeName;
            AttributeValue = value;
        }
    }

    #if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(ShowConditionalAttribute))]
    public class ShowConditionalDrawer : PropertyDrawer
    {
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUI.GetPropertyHeight(property);
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var showConditional = attribute as ShowConditionalAttribute;

            var index = property.propertyPath.LastIndexOf(".");
            var path = property.propertyPath.Substring(0, index + 1) + showConditional.AttributeName;

            var relatedProperty = property.serializedObject.FindProperty(path);

            var disabled = false;

            if (relatedProperty == null)
            {
                EditorGUI.PropertyField(position, property);
                return;
            }
            switch (relatedProperty.propertyType)
            {
                case SerializedPropertyType.Boolean:
                    disabled = relatedProperty.boolValue != (bool)showConditional.AttributeValue;
                    break;
                case SerializedPropertyType.Enum:
                    disabled = relatedProperty.enumValueIndex != (int)showConditional.AttributeValue;
                    break;
                case SerializedPropertyType.Integer:
                    disabled = relatedProperty.intValue != (int)showConditional.AttributeValue;
                    break;
                case SerializedPropertyType.Float:
                    disabled = relatedProperty.floatValue != (float)showConditional.AttributeValue;
                    break;
                case SerializedPropertyType.ObjectReference:
                    disabled = relatedProperty.objectReferenceValue != (Object)showConditional.AttributeValue;
                    break;
                case SerializedPropertyType.String:
                    disabled = relatedProperty.stringValue.Presence() != ((string)showConditional.AttributeValue).Presence();
                    break;
                default:
                    Debug.LogFormat("ShowConditionalDrawer: unsupported type {0}", relatedProperty.propertyType);
                    break;
            }
            EditorGUI.BeginDisabledGroup(disabled);
            if (property.hasChildren)
            {
                EditorGUI.PropertyField(position, property, label, true);
            }
            else
            {
                EditorGUI.PropertyField(position, property, label);
            }
            EditorGUI.EndDisabledGroup();
        }
    }
    #endif
}
