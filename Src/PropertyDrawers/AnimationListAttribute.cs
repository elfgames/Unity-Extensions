﻿using System;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
using ElfGames.Extensions.Editor;
#endif

namespace ElfGames.Extensions
{
    /// <summary>
    /// Draws a popup with all the animations in the Animator component of the selected GameObject.
    /// </summary>
    public class AnimationListAttribute : PropertyAttribute { }

#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(AnimationListAttribute))]
    public class AnimationListDrawer : PropertyDrawer
    {
        string[] animationNames;
        UnityEngine.Object lastSelected;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            UpdateAnimationsCache(property);
            if (animationNames == null)
            {
                EditorGUI.PropertyField(position, property, label);
                return;
            }

            if (property.propertyType == SerializedPropertyType.String)
            {
                int index = Mathf.Max(0, Array.IndexOf(animationNames, property.stringValue));
                EditorGUI.BeginChangeCheck();
                index = EditorGUI.Popup(position, property.displayName, index, animationNames);
                if (EditorGUI.EndChangeCheck())
                {
                    property.stringValue = animationNames[index];
                    property.serializedObject.ApplyModifiedProperties();
                }
            }
            else if (property.propertyType == SerializedPropertyType.Integer)
            {
                var name = animationNames[property.intValue];
                int index = Mathf.Clamp(property.intValue, 0, animationNames.Length);
                EditorGUI.BeginChangeCheck();
                index = EditorGUI.Popup(position, property.displayName, index, animationNames);
                if (EditorGUI.EndChangeCheck())
                {
                    property.intValue = index;
                    property.serializedObject.ApplyModifiedProperties();
                }
            }
            else
            {
                base.OnGUI(position, property, label);
            }
        }

        void UpdateAnimationsCache(SerializedProperty property)
        {
            if (lastSelected != Selection.activeObject)
            {
                animationNames = null;
                lastSelected = Selection.activeObject;
            }

            if (animationNames == null)
            {
                LoadAndCacheAnimations(property);
            }
        }

        void LoadAndCacheAnimations(SerializedProperty property) {
            var animatorController = AnimatorUtils.FindSomeKindOfAnimatorController(property);

            if (animatorController == null) {
                return;
            }

            animationNames = AnimatorUtils.GetAnimations(animatorController);
        }
    }
#endif
}
