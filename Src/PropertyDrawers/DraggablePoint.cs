﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace ElfGames.Extensions
{

    /// <summary>
    /// A Property attribute that draws a draggable point in the scene view based on the property value.
    /// </summary>
    public class DraggablePoint : ScenePropertyAttribute
    {
        public enum Refs
        {
            World, // Absolute coordinates
            RelativePosition, // coordinates relative to transform position only
            Relative, // Considers transform position/scale/rotation
            Parent // coordinates relative to the parent transform position/scale/rotation
        }

        protected Refs Reference;
        protected Color HandleColor = Color.blue;

        [System.Obsolete]
        public DraggablePoint(bool relative)
        {
            Reference = relative ? Refs.RelativePosition : Refs.World;
        }

        public DraggablePoint(Refs reference = Refs.World)
        {
            Reference = reference;
        }

    #if UNITY_EDITOR
        public override void OnSceneGUI(SerializedProperty property)
        {
            if (property.serializedObject.isEditingMultipleObjects)
            {
                return;
            }
            var scriptObject = property.serializedObject.targetObject as MonoBehaviour;

            DragProperty(property, property, scriptObject.transform);

            if (property.propertyType == SerializedPropertyType.Generic)
            {
                for (var i = 0; i < property.arraySize; i++)
                {
                    var el = property.GetArrayElementAtIndex(i);
                    DragProperty(el, i > 0 ? property.GetArrayElementAtIndex(i - 1) : el, scriptObject.transform);
                }
            }
        }

        void DragProperty(SerializedProperty property, SerializedProperty previous, Transform transform)
        {
            switch (property.propertyType)
            {
                case SerializedPropertyType.Vector3:
                    property.vector3Value = DragPoint(property.propertyPath, property.vector3Value, previous.vector3Value, transform);
                    break;
                case SerializedPropertyType.Vector2:
                    property.vector2Value = DragPoint(property.propertyPath, property.vector2Value, previous.vector2Value, transform);
                    break;
                case SerializedPropertyType.Bounds:
                    var b = property.boundsValue;
                    b.center = DragPoint(property.propertyPath, b.center, b.center, transform);
                    property.boundsValue = b;
                    break;
                case SerializedPropertyType.Rect:
                    var r = property.rectValue;
                    r.position = DragPoint(property.propertyPath, r.position, r.position, transform);
                    Handles.DrawLine(r.min, r.min + Vector2.right * r.width);
                    Handles.DrawLine(r.min, r.min + Vector2.up * r.height);
                    Handles.DrawLine(r.max, r.max - Vector2.up * r.height);
                    Handles.DrawLine(r.max, r.max - Vector2.right * r.width);
                    r.max = DragPoint(property.propertyPath, r.max, r.max, transform);
                    property.rectValue = r;
                    break;
            }
        }

        protected Vector3 DragPoint(string name, Vector3 position, Vector3 previous, Transform transform)
        {
            Handles.color = HandleColor;
            if (Reference == Refs.Relative)
            {
                var absPosition = transform.localToWorldMatrix.MultiplyPoint(position);
                Handles.Label(absPosition, PrettifyName(name));
                Handles.DrawLine(absPosition, transform.localToWorldMatrix.MultiplyPoint(previous));
                return transform.worldToLocalMatrix.MultiplyPoint(Handles.PositionHandle(absPosition, Quaternion.identity));
            }
            else if (Reference == Refs.RelativePosition)
            {
                var absPosition = position + transform.position;
                Handles.Label(absPosition, PrettifyName(name));
                Handles.DrawLine(absPosition, previous + transform.position);
                return Handles.PositionHandle(absPosition, Quaternion.identity) - transform.position;
            }
            else if (Reference == Refs.Parent)
            {
                var absPosition = transform.parent.localToWorldMatrix.MultiplyPoint(position);
                Handles.Label(absPosition, PrettifyName(name));
                Handles.DrawLine(absPosition, transform.parent.localToWorldMatrix.MultiplyPoint(previous));
                return transform.parent.worldToLocalMatrix.MultiplyPoint(Handles.PositionHandle(absPosition, Quaternion.identity));
            }
            else
            {
                Handles.Label(position, PrettifyName(name));
                Handles.DrawLine(position, previous);
                return Handles.PositionHandle(position, Quaternion.identity);
            }
        }

        protected string PrettifyName(string name)
        {
            return name.Replace(".Array.data[", "[");
        }
    #endif
    }
}
