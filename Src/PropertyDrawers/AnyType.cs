﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace ElfGames.Extensions
{
#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(NumericType))]
    public class NumericTypeDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            // Using BeginProperty / EndProperty on the parent property means that
            // prefab override logic works on the entire property.
            EditorGUI.BeginProperty(position, label, property);

            // Draw label
            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

            // Don't make child fields be indented
            var indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            RenderGenericType(position, property);

            // Set indent back to what it was
            EditorGUI.indentLevel = indent;

            EditorGUI.EndProperty();
        }

        private void RenderGenericType(Rect position, SerializedProperty property)
        {
            // Calculate rects
            var amountRect = new Rect(position.x, position.y, position.width / 3, position.height);
            var unitRect = new Rect(position.x + position.width / 3, position.y, position.width / 3 * 2, position.height);

            var type = property.FindPropertyRelative("Type");
            var numeric = property.FindPropertyRelative("NumericValue");

            EditorGUI.PropertyField(amountRect, type, GUIContent.none);
            switch ((SupportedTypes)type.enumValueIndex)
            {
                case SupportedTypes.Float:
                    EditorGUI.BeginChangeCheck();
                    var floatValue = EditorGUI.FloatField(unitRect, numeric.vector4Value.x);
                    if (EditorGUI.EndChangeCheck())
                    {
                        numeric.vector4Value = new Vector3(floatValue, 0, 0);
                        numeric.serializedObject.ApplyModifiedProperties();
                    }
                    break;
                case SupportedTypes.Boolean:
                    EditorGUI.BeginChangeCheck();
                    var boolean = EditorGUI.Toggle(unitRect, (int)numeric.vector4Value.x != 0);
                    if (EditorGUI.EndChangeCheck())
                    {
                        numeric.vector4Value = new Vector3(boolean ? 1 : 0, 0, 0);
                        numeric.serializedObject.ApplyModifiedProperties();
                    }
                    break;
                case SupportedTypes.Integer:
                    EditorGUI.BeginChangeCheck();
                    var integer = EditorGUI.IntField(unitRect, (int)numeric.vector4Value.x);
                    if (EditorGUI.EndChangeCheck())
                    {
                        numeric.vector4Value = new Vector3(integer, 0, 0);
                        numeric.serializedObject.ApplyModifiedProperties();
                    }
                    break;
                case SupportedTypes.Vector2:
                    EditorGUI.BeginChangeCheck();
                    var vector2 = EditorGUI.Vector2Field(unitRect, GUIContent.none, numeric.vector4Value);
                    if (EditorGUI.EndChangeCheck())
                    {
                        numeric.vector4Value = vector2;
                        numeric.serializedObject.ApplyModifiedProperties();
                    }
                    break;
                case SupportedTypes.Vector3:
                    EditorGUI.BeginChangeCheck();
                    var vector3 = EditorGUI.Vector3Field(unitRect, GUIContent.none, numeric.vector4Value);
                    if (EditorGUI.EndChangeCheck())
                    {
                        numeric.vector4Value = vector3;
                        numeric.serializedObject.ApplyModifiedProperties();
                    }
                    break;
                case SupportedTypes.Color:
                    EditorGUI.BeginChangeCheck();
                    var color = EditorGUI.ColorField(unitRect, GUIContent.none, numeric.vector4Value);
                    if (EditorGUI.EndChangeCheck())
                    {
                        numeric.vector4Value = color;
                        numeric.serializedObject.ApplyModifiedProperties();
                    }
                    break;
                case SupportedTypes.Vector4:
                    EditorGUI.BeginChangeCheck();
                    var vector4 = EditorGUI.Vector4Field(unitRect, GUIContent.none, numeric.vector4Value);
                    if (EditorGUI.EndChangeCheck())
                    {
                        numeric.vector4Value = vector4;
                        numeric.serializedObject.ApplyModifiedProperties();
                    }
                    break;
                default:
                    RenderNullableTypes((SupportedTypes)type.enumValueIndex, property, unitRect);
                    break;
            }
        }

        protected virtual void RenderNullableTypes(SupportedTypes enumValueIndex, SerializedProperty property, Rect unitRect)
        {
            EditorGUI.LabelField(unitRect, "Not supported.");
        }
    }

    [CustomPropertyDrawer(typeof(AnyType))]
    public class AnyTypeDrawer : NumericTypeDrawer
    {
        protected override void RenderNullableTypes(SupportedTypes type, SerializedProperty property, Rect unitRect)
        {
            switch (type)
            {
                case SupportedTypes.String:
                    EditorGUI.PropertyField(unitRect, property.FindPropertyRelative("StringValue"), GUIContent.none);
                    break;
                default:
                case SupportedTypes.Object:
                    EditorGUI.PropertyField(unitRect, property.FindPropertyRelative("ObjectValue"), GUIContent.none);
                    break;
            }
        }
    }
#endif

    public enum SupportedTypes { String, Float, Integer, Vector2, Vector3, Vector4, Color, Object, Boolean }

    [System.Serializable]
    public class NumericType
    {
        public SupportedTypes Type;
        public Vector4 NumericValue;

        public Color ColorValue
        {
            get
            {
                return NumericValue;
            }
        }

        public bool BoolValue
        {
            get
            {
                return (int)NumericValue.x != 0;
            }
        }

        public int IntValue
        {
            get
            {
                return (int)NumericValue.x;
            }
        }

        public float FloatValue
        {
            get
            {
                return NumericValue.x;
            }
        }

        public virtual object Value
        {
            get
            {
                switch (Type)
                {
                    case SupportedTypes.Float:
                        return NumericValue.x;
                    case SupportedTypes.Integer:
                        return (int)NumericValue.x;
                    case SupportedTypes.Vector2:
                        return (Vector2)NumericValue;
                    case SupportedTypes.Vector3:
                        return (Vector3)NumericValue;
                    case SupportedTypes.Vector4:
                        return NumericValue;
                    case SupportedTypes.Color:
                        return (Color)NumericValue;
                    case SupportedTypes.Boolean:
                        return BoolValue;
                    default:
                        return null;
                }
            }
        }
    }

    /// <summary>
    /// A type that can be any of the supported types.
    /// </summary>
    [System.Serializable]
    public class AnyType : NumericType
    {
        public string StringValue;
        public Object ObjectValue;

        public override object Value
        {
            get
            {
                var result = base.Value;
                if (result != null)
                {
                    return result;
                }
                switch (Type)
                {
                    case SupportedTypes.String:
                        return StringValue;
                    case SupportedTypes.Object:
                    default:
                        return ObjectValue;
                }
            }
        }
    }
}
