﻿using System;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.Animations;
using System.Linq;
using ElfGames.Extensions.Editor;
#endif

namespace ElfGames.Extensions
{
    /// <summary>
    /// Draws a popup with all the parameters of the Animator on the same GameObject or any of its children.
    /// </summary>
    public class AnimatorParameterAttribute : PropertyAttribute { }


    #if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(AnimatorParameterAttribute))]
    public class AnimatorParameterDrawer : PropertyDrawer
    {
        string[] parameters;
        private bool needRefresh = true;
        AnimatorController animatorController;
        UnityEngine.Object lastSelected;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            LoadAndCacheAnimatorParameters(property);
            if (parameters == null)
            {
                EditorGUI.PropertyField(position, property, label);
                return;
            }

            if (property.propertyType == SerializedPropertyType.String)
            {
                int index = Array.IndexOf(parameters, property.stringValue);
                EditorGUI.BeginChangeCheck();
                index = EditorGUI.Popup(position, property.displayName, index, parameters);
                if (EditorGUI.EndChangeCheck())
                {
                    property.stringValue = index >= 0 ? parameters[index] : null;
                    property.serializedObject.ApplyModifiedProperties();
                }
            }
            else if (property.propertyType == SerializedPropertyType.Integer)
            {
                int index = Array.IndexOf(parameters, property.stringValue);
                EditorGUI.BeginChangeCheck();
                index = EditorGUI.Popup(position, property.displayName, index, parameters);
                if (EditorGUI.EndChangeCheck())
                {
                    property.intValue = index >= 0 ? animatorController.parameters[index].nameHash : -1;
                    property.serializedObject.ApplyModifiedProperties();
                }
            }
            else
            {
                base.OnGUI(position, property, label);
            }
        }

        void LoadAndCacheAnimatorParameters(SerializedProperty property)
        {
            if (lastSelected != Selection.activeObject)
            {
                needRefresh = true;
                lastSelected = Selection.activeObject;
            }
            if (needRefresh)
            {
                ReloadParams(property);
                needRefresh = false;
            }
        }

        void ReloadParams(SerializedProperty property) {
            var animatorController = AnimatorUtils.FindSomeKindOfAnimatorController(property);

            if (animatorController == null) {
                return;
            }

            parameters = AnimatorUtils.GetParameters(animatorController);
        }
    }
    #endif
}
