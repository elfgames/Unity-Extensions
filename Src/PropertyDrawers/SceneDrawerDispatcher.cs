﻿using System;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
using System.Reflection;
#endif

namespace ElfGames.Extensions
{

    /// <summary>
    ///   An abstract class that allows you to draw a custom scene GUI for a property.
    /// </summary>
    public abstract class ScenePropertyAttribute : PropertyAttribute
    {
#if UNITY_EDITOR
    public abstract void OnSceneGUI(SerializedProperty property);
#endif
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(MonoBehaviour), true)]
    public class SceneDrawerDispatcher : UnityEditor.Editor
    {

        readonly GUIStyle style = new GUIStyle();

        public void OnEnable()
        {
            style.fontStyle = FontStyle.Bold;
            style.normal.textColor = Color.white;
        }

        public void OnSceneGUI()
        {
            if (!target)
            {
                return;
            }
            if (!UnityEditorInternal.InternalEditorUtility.GetIsInspectorExpanded(target as Component))
            {
                return;
            }

            var property = serializedObject.GetIterator();
            while (property.NextVisible(true))
            {
                RenderNestedPropertyDrawers(property);
            }
        }

        void RenderNestedPropertyDrawers(SerializedProperty property) {
            if (CheckTypeAttribute(target.GetType(), property))
            {
                return;
            }
            if (property.hasChildren && property.isExpanded && property.propertyType == SerializedPropertyType.Generic && property.type != "Array")
            {
                var type = Type.GetType(property.type) ?? target.GetType().GetNestedType(property.type);
                if (type == null)
                {
                    return;
                }
                var myChildren = property.Copy();

                var hasInnerProperty = myChildren.NextVisible(true);

                while (hasInnerProperty)
                {
                    CheckTypeAttribute(type, myChildren);
                    hasInnerProperty = myChildren.NextVisible(false);
                }
            }
        }

        bool CheckTypeAttribute(Type type, SerializedProperty property)
        {
            var field = type.GetField(property.name, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            if (field != null)
            {
                var attributes = field.GetCustomAttributes(typeof(ScenePropertyAttribute), true);
                foreach (var a in attributes)
                {
                    ((ScenePropertyAttribute)a).OnSceneGUI(property);
                }
                if (attributes.Length > 0)
                {
                    serializedObject.ApplyModifiedProperties();
                    return true;
                }
            }
            return false;
        }
    }
#endif
}
