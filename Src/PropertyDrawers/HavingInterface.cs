﻿using System;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace ElfGames.Extensions
{
    /// <summary>
    /// A property drawer that checks if the assigned object has the specified interface.
    /// </summary>
    public class HavingInterface : PropertyAttribute
    {
        public Type Type { get; private set; }

        public HavingInterface(Type name)
        {
            Type = name;
        }
    }


    #if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(HavingInterface))]
    public class HavingInterfaceDrawer : PropertyDrawer {


        // Draw the property inside the given rect
        public override void OnGUI (Rect position, SerializedProperty property, GUIContent label) {
            var type = ((HavingInterface)attribute).Type;
            EditorGUI.ObjectField(position, property, typeof(GameObject));
            if (property.objectReferenceValue != null && !type.IsInstanceOfType(property.objectReferenceValue)) {
                property.objectReferenceValue = null;
                Debug.LogError ("Invalid object type");
            }
        }
    }
    #endif
}
