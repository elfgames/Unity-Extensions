﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
using ElfGames.Extensions.Editor;
#endif

namespace ElfGames.Extensions
{
    /// <summary>
    ///   A reference to an animation in an animator. Used to play animations in the inspector.
    /// </summary>
    [Serializable]
    public class AnimationRef
    {
        [SerializeField] protected Animator animatorReference;
        [SerializeField] protected string animationName;

        public Animator Animator => animatorReference;
        public string Animation => animationName;

        public bool IsPlayable => animatorReference != null && !animationName.IsNullOrEmpty();

        public void Play()
        {
            Animator.Play(Animation);
        }

        public IEnumerator PlayAndWait()
        {
            return Animator.PlayAndWait(Animation);
        }
    }

    #if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(AnimationRef))]
    public class AnimationRefDrawer : PropertyDrawer
    {
        readonly Dictionary<int, string[]> animationsDic = new Dictionary<int, string[]>();
        UnityEngine.Object lastSelected;

        const float MARGIN = 4;

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return base.GetPropertyHeight(property, label) * 3 + MARGIN;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var size = (position.height - MARGIN);

            var labelPosition = new Rect(position.x, position.y, position.width, size / 3f);
            EditorGUI.LabelField(labelPosition, property.displayName, EditorStyles.boldLabel);

            EditorGUI.indentLevel++;

            var animator = property.FindPropertyRelative("animatorReference");
            var animation = property.FindPropertyRelative("animationName");

            var animatorPosition = new Rect(position.x, position.y + size / 3f, position.width, size / 3);
            EditorGUI.PropertyField(animatorPosition, animator);

            var animationNames = LoadAndCacheAnimations(animator);
            if (animationNames == null)
            {
                EditorGUI.indentLevel--;

                return;
            }

            var animationPosition = new Rect(position.x, position.y + size * 2 / 3f, position.width, size / 2);
            int index = Mathf.Max(0, Array.IndexOf(animationNames, animation.stringValue));
            EditorGUI.BeginChangeCheck();
            index = EditorGUI.Popup(animationPosition, animation.displayName, index, animationNames);
            if (EditorGUI.EndChangeCheck())
            {
                animation.stringValue = animationNames[index];
                animation.serializedObject.ApplyModifiedProperties();
            }
            EditorGUI.indentLevel--;
        }

        string[] LoadAndCacheAnimations(SerializedProperty property)
        {
            if (lastSelected != Selection.activeObject)
            {
                animationsDic.Clear(); ;
                lastSelected = Selection.activeObject;
            }
            var component = property.objectReferenceValue as Animator;
            if (component == null)
            {
                return null;
            }

            if (!animationsDic.ContainsKey(component.GetHashCode()))
            {
                animationsDic[component.GetHashCode()] = AnimatorUtils.GetAnimations(component);
            }
            return animationsDic[component.GetHashCode()];
        }
    }
    #endif
}
