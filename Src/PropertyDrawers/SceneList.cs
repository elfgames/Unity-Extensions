﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
using UnityEngine.SceneManagement;
using System.IO;
#endif

namespace ElfGames.Extensions
{
    /// <summary>
    /// A property drawer that displays a popup with a list of scenes in the build settings.
    /// </summary>
    public class SceneList : PropertyAttribute
    {
    }

#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(SceneList))]
    public class SceneListDrawer : PropertyDrawer
    {
    string[] scenes;

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        if (property.propertyType == SerializedPropertyType.String)
        {
        EditorGUI.BeginChangeCheck();
        if (scenes == null || scenes.Length != SceneManager.sceneCount) {
            BuildSceneNames();
        }
        var index = System.Array.IndexOf(scenes, property.stringValue);
        index = EditorGUILayout.Popup(property.displayName, index, scenes);
        if (EditorGUI.EndChangeCheck())
        {
            property.stringValue = scenes[index];
            property.serializedObject.ApplyModifiedProperties();
            BuildSceneNames();
        }
        }
    }

    void BuildSceneNames() {
        scenes = new string[SceneManager.sceneCountInBuildSettings];

        for (var i = 0; i < scenes.Length; i++)
        {
        scenes[i] = Path.GetFileNameWithoutExtension(SceneUtility.GetScenePathByBuildIndex(i));
        }
    }
    }
#endif
}
