﻿using System;
using System.Reflection;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace ElfGames.Extensions
{

    /// <summary>
    /// Draws a popup with a list of strings computed at runtime from a method.
    /// </summary>
    public class ComputedListPopup : PropertyAttribute
    {
        public ComputedListPopup(string methodName)
        {
            MethodName = methodName;
        }

        public string MethodName
        {
            get;
            private set;
        }

        public string[] GetItems(object item)
        {
            var method = item.GetType().GetMethod(MethodName, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.DeclaredOnly | BindingFlags.Public);
            if (method == null)
            {
                return new string[] { };
            }
            if (method.ReturnType != typeof(string[]))
            {
                return new string[] { };
            }

            if (method.GetParameters().Length != 0)
            {
                return new string[] { };
            }

            return method.Invoke(item, null) as string[];
        }
    }

    #if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(ComputedListPopup))]
    public class ComputedListPopupDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var attributeValue = attribute as ComputedListPopup;
            var list = attributeValue.GetItems(property.serializedObject.targetObject);
            if (property.propertyType == SerializedPropertyType.String)
            {
                int index = Mathf.Max(0, Array.IndexOf(list, property.stringValue));
                index = EditorGUI.Popup(position, property.displayName, index, list);

                property.stringValue = list[index];
            }
            else if (property.propertyType == SerializedPropertyType.Integer)
            {
                property.intValue = EditorGUI.Popup(position, property.displayName, property.intValue, list);
            }
            else
            {
                base.OnGUI(position, property, label);
            }
        }
    }
    #endif
}
