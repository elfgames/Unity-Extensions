﻿using System;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
using System.Reflection;
using UnityEditorInternal;
#endif

namespace ElfGames.Extensions
{
    /// <summary>
    /// A property drawer that displays a popup with a list of sorting layers.
    /// </summary>
    public class SortingLayerName : PropertyAttribute { }


#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(SortingLayerName))]
    public class SortingLayerNameDrawer : PropertyDrawer {

        static string[] _layerNames;
        static string[] LayerNames {
            get {
                if (_layerNames == null) {
                    Type internalEditorUtilityType = typeof(InternalEditorUtility);
                    PropertyInfo sortingLayersProperty = internalEditorUtilityType.GetProperty ("sortingLayerNames", BindingFlags.Static | BindingFlags.NonPublic);
                    _layerNames = (string[])sortingLayersProperty.GetValue (null, new object[0]);
                }
                return _layerNames;
            }
        }

        // Draw the property inside the given rect
        public override void OnGUI (Rect position, SerializedProperty property, GUIContent label) {
            if (property.propertyType == SerializedPropertyType.String) {
                int index = Mathf.Max (0, Array.IndexOf (LayerNames, property.stringValue));
                index = EditorGUI.Popup (position, property.displayName, index, LayerNames);

                property.stringValue = LayerNames [index];
            } else if (property.propertyType == SerializedPropertyType.Integer) {
                var name = SortingLayer.IDToName (property.intValue);
                int index = Mathf.Max (0, Array.IndexOf (LayerNames, name));
                index = EditorGUI.Popup (position, property.displayName, index, LayerNames);
                property.intValue = SortingLayer.NameToID (LayerNames [index]);
            } else {
                base.OnGUI (position, property, label);
            }
        }
    }
#endif
}
