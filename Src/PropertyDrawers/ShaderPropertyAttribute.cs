﻿using System;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace ElfGames.Extensions
{
    /// <summary>
    ///     A property drawer that shows a popup with a list of shader properties.
    /// </summary>
    public class ShaderPropertyAttribute : PropertyAttribute { }


#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(ShaderPropertyAttribute))]
    public class ShaderPropertyDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (property.propertyType == SerializedPropertyType.String)
            {
                var shaderPropertyNames = LoadShaderProperties(position, property);
                if (shaderPropertyNames == null)
                {
                    return;
                }

                int index = Mathf.Max(0, Array.IndexOf(shaderPropertyNames, property.stringValue));
                index = EditorGUI.Popup(position, property.displayName, index, shaderPropertyNames);

                property.stringValue = shaderPropertyNames[index];
            }
            else
            {
                base.OnGUI(position, property, label);
            }
        }

        string[] LoadShaderProperties(Rect position, SerializedProperty property)
        {
            var component = property.serializedObject.targetObject as Component;
            if (component == null)
            {
                ErrorMessage(position, "Shader properties only work on a single component");
                return null;
            }

            var renderer = component.GetComponent<Renderer>();
            var material = renderer == null ? null : renderer.sharedMaterial;

            if (renderer == null)
            {
                var graphic = component.GetComponent<UnityEngine.UI.Graphic>();
                if (graphic != null && graphic.material != null)
                {
                    material = graphic.material;
                }
                else
                {
                    ErrorMessage(position, "No such Renderer");
                    return null;
                }
            }

            if (material == null)
            {
                ErrorMessage(position, "No such Material");
                return null;
            }
            var shader = material.shader;

            var shaderPropertyNames = new string[ShaderUtil.GetPropertyCount(shader)];
            for (var i = 0; i < shaderPropertyNames.Length; i++)
            {
                shaderPropertyNames[i] = ShaderUtil.GetPropertyName(shader, i);
            }
            return shaderPropertyNames;
        }

        void ErrorMessage(Rect position, string message)
        {
            EditorGUI.LabelField(position, message);
        }

    }
#endif
}
