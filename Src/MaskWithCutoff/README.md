Mask With Alpha Cutoff
======================

This component is a replacement for the default UnityEngine.UI.Mask component, that implements alpha cutoff parameter, like SpriteMasks does.

This is very useful to implement fillable bars using the alpha cutoff to fill a mask.

To use it, add the component together with a Graphic (i.e. Image) component to a RectTransform.

Finally assign the UIMask material to the Graphic component.
