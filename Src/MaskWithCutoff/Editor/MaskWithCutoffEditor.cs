#if UNITY_EDITOR
using UnityEngine.UI;

namespace UnityEditor.UI
{
    [CustomEditor(typeof(MaskWithCutoff))]
    [CanEditMultipleObjects]
    public class MaskWithCutoffEditor : Editor
    {
        SerializedProperty m_ShowMaskGraphic;
        SerializedProperty m_AlphaCutoff;

        protected virtual void OnEnable()
        {
            m_ShowMaskGraphic = serializedObject.FindProperty("m_ShowMaskGraphic");
            m_AlphaCutoff = serializedObject.FindProperty("m_AlphaCutoff");
        }

        public override void OnInspectorGUI()
        {
            var graphic = (target as Mask).GetComponent<Graphic>();

            if (graphic && !graphic.IsActive())
                EditorGUILayout.HelpBox("Masking disabled due to Graphic component being disabled.", MessageType.Warning);

            serializedObject.Update();
            EditorGUILayout.PropertyField(m_ShowMaskGraphic);
            EditorGUILayout.PropertyField(m_AlphaCutoff);
            serializedObject.ApplyModifiedProperties();
        }
    }
}
#endif
