﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

namespace ElfGames.Extensions
{
    public class ThreadedAction : CustomYieldInstruction
    {
        readonly Thread thread;

        public ThreadedAction(ThreadStart threadedAction)
        {
            thread = new Thread(threadedAction);
            thread.Start();
        }

        public override bool keepWaiting
        {
            get
            {
                return thread.IsAlive;
            }
        }
    }
}
