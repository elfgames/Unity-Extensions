﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ElfGames.Extensions
{
    public class WaitAction<T> : CustomYieldInstruction
    {
        public bool Resolved { get; private set; }
        public T Output { get; private set; }

        public void OnEvent(T data)
        {
            Output = data;
            Resolved = true;
        }

        public override bool keepWaiting
        {
            get
            {
                return !Resolved;
            }
        }
    }
}
