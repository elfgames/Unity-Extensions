﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ElfGames.Extensions
{
    public interface IEventListener
    {
        bool Resolved { get; }
    }

    public class EventListener : IEventListener
    {

        public bool Resolved
        {
            get;
            protected set;
        }

        public virtual void ReceiveEvent()
        {
            if (Resolved)
            {
                throw new System.Exception("Can't receive an event multiple times");
            }
            Resolved = true;
        }

        public void Reset()
        {
            Resolved = false;
        }
    }

    public class EventListener<T> : IEventListener
    {

        public bool Resolved
        {
            get;
            protected set;
        }

        public T Data
        {
            get;
            protected set;
        }

        public virtual void ReceiveEvent(T data)
        {
            if (Resolved)
            {
                throw new System.Exception("Can't receive an event multiple times");
            }
            Resolved = true;
            Data = data;
        }

        public void Reset()
        {
            Resolved = false;
            Data = default(T);
        }
    }

    public class WaitForEvent : CustomYieldInstruction
    {
        readonly IEventListener listener;

        public WaitForEvent(IEventListener listener)
        {
            this.listener = listener;
        }

        public override bool keepWaiting
        {
            get
            {
                return !listener.Resolved;
            }
        }
    }
}
