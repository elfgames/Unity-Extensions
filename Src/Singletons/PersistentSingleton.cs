﻿using UnityEngine;

namespace ElfGames.Extensions
{

    public class PersistentSingleton<T> : MonoBehaviour, ISingleton where T : Component
    {
        static T _instance;

        public static bool IsAwake { get { return (_instance != null); } }

        public static T Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType(typeof(T)) as T;
                }
                return _instance;
            }
        }

        protected virtual void Awake()
        {
            _instance = this as T;
            DontDestroyOnLoad(gameObject);
        }
    }
}
