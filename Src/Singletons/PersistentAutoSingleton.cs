﻿using UnityEngine;

namespace ElfGames.Extensions
{
    // <summary>
    //     A singleton that persists across scenes and self-instantiates if it doesn't exist.
    //     This is useful for things like a game manager.
    // </summary>
    public class PersistentAutoSingleton<T> : MonoBehaviour
    where T : Component
    {
        public static bool IsAwake { get { return (_instance != null); } }

        static T _instance;
        public static T Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType(typeof(T)) as T;
                    if (_instance == null)
                    {
                        var obj = new GameObject(nameof(T));
                        obj.hideFlags = HideFlags.HideAndDontSave;
                        _instance = obj.AddComponent(typeof(T)) as T;
                    }
                }
                return _instance;
            }
        }

        protected virtual void Awake()
        {
            DontDestroyOnLoad(gameObject);
            if (_instance == null)
            {
                _instance = this as T;
            }
            else
            {
                Destroy(gameObject);
            }
        }
    }
}
