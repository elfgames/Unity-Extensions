﻿using System.Collections.Generic;
using UnityEngine;

namespace ElfGames.Extensions
{
    [System.Serializable]
    public class ShaderOption
    {
        [ShaderProperty] public string Name;
        public AnyType Value;

        public void SetPropertyValue(MaterialPropertyBlock properties)
        {
            switch (Value.Type)
            {
                case SupportedTypes.Integer:
                    properties.SetInt(Name, Value.IntValue);
                    break;
                case SupportedTypes.Color:
                    properties.SetColor(Name, Value.ColorValue);
                    break;
                case SupportedTypes.Vector2:
                case SupportedTypes.Vector3:
                case SupportedTypes.Vector4:
                    properties.SetVector(Name, Value.NumericValue);
                    break;
                case SupportedTypes.Float:
                    properties.SetFloat(Name, Value.FloatValue);
                    break;
                case SupportedTypes.Boolean:
                    properties.SetFloat(Name, Value.BoolValue ? 1 : 0);
                    break;
                case SupportedTypes.Object:
                    var textureValue = Value.ObjectValue as Texture;
                    if (textureValue != null)
                    {
                        properties.SetTexture(Name, textureValue);
                    }
                    break;
            }
        }

        public void SetPropertyValue(Material properties)
        {
            switch (Value.Type)
            {
                case SupportedTypes.Integer:
                    properties.SetInt(Name, Value.IntValue);
                    break;
                case SupportedTypes.Color:
                    properties.SetColor(Name, Value.ColorValue);
                    break;
                case SupportedTypes.Vector2:
                case SupportedTypes.Vector3:
                case SupportedTypes.Vector4:
                    properties.SetVector(Name, Value.NumericValue);
                    break;
                case SupportedTypes.Float:
                    properties.SetFloat(Name, Value.FloatValue);
                    break;
                case SupportedTypes.Boolean:
                    properties.SetFloat(Name, Value.BoolValue ? 1 : 0);
                    break;
                case SupportedTypes.Object:
                    var textureValue = Value.ObjectValue as Texture;
                    if (textureValue != null)
                    {
                        properties.SetTexture(Name, textureValue);
                    }
                    break;
            }
        }
    }

    [ExecuteInEditMode]
    [RequireComponent(typeof(Renderer))]
    public class ShaderOptionsHelper : MonoBehaviour, IShaderOptionsHelper
    {
        [SerializeField] protected ShaderOption[] Options;
        private Renderer _renderer;

        MaterialPropertyBlock currentPropertyBlock;

        readonly Dictionary<string, ShaderOption> Overrides = new Dictionary<string, ShaderOption>();

        void Awake()
        {
            _renderer = GetComponent<Renderer>();
            UpdateShaderOptions();
        }

        void UpdateShaderOptions()
        {
            if (_renderer == null || Options == null)
            {
                return;
            }
            currentPropertyBlock = new MaterialPropertyBlock();
            _renderer.GetPropertyBlock(currentPropertyBlock);
            if (!_renderer.sharedMaterial)
            {
                return;
            }

            foreach (var element in Options)
            {
                if (!_renderer.sharedMaterial.HasProperty(element.Name))
                {
                    Debug.LogErrorFormat(this, "No such property {0} in {1}", element.Name, gameObject.name);
                    continue;
                }
                element.SetPropertyValue(currentPropertyBlock);
            }

            foreach (var element in Overrides)
            {
                if (!_renderer.sharedMaterial.HasProperty(element.Key))
                {
                    Debug.LogErrorFormat(this, "No such property {0} in {1}", element.Key, gameObject.name);
                    continue;
                }
                element.Value.SetPropertyValue(currentPropertyBlock);
            }
            _renderer.SetPropertyBlock(currentPropertyBlock);
        }

        public float GetFloat(string propertyName)
        {
            return currentPropertyBlock.GetFloat(propertyName);
        }

        public int GetInt(string propertyName)
        {
            return currentPropertyBlock.GetInt(propertyName);
        }

        public Color GetColor(string propertyName)
        {
            return currentPropertyBlock.GetColor(propertyName);
        }

        public Vector4 GetVector(string propertyName)
        {
            return currentPropertyBlock.GetVector(propertyName);
        }

        public Texture GetTexture(string propertyName)
        {
            return currentPropertyBlock.GetTexture(propertyName);
        }

        public void SetFloat(string propertyName, float value)
        {
            LoadValue(propertyName, SupportedTypes.Float).NumericValue.x = value;
            UpdateShaderOptions();
        }

        public void SetInt(string propertyName, int value)
        {
            LoadValue(propertyName, SupportedTypes.Integer).NumericValue.x = value;
            UpdateShaderOptions();
        }

        public void SetColor(string propertyName, Color value)
        {
            LoadValue(propertyName, SupportedTypes.Color).NumericValue = value;
            UpdateShaderOptions();
        }

        public void SetVector(string propertyName, Vector4 value)
        {
            LoadValue(propertyName, SupportedTypes.Vector4).NumericValue = value;
            UpdateShaderOptions();
        }

        public void SetTexture(string propertyName, Texture value)
        {
            LoadValue(propertyName, SupportedTypes.Object).ObjectValue = value;
            UpdateShaderOptions();
        }

        public void SetOption(ShaderOption option)
        {
            Overrides[option.Name] = option;
            UpdateShaderOptions();
        }

        public void UnsetOption(ShaderOption option)
        {
            Overrides.Remove(option.Name);
            UpdateShaderOptions();
        }

        AnyType LoadValue(string propertyName, SupportedTypes type)
        {
            ShaderOption option;
            if (Overrides.ContainsKey(propertyName))
            {
                option = Overrides[propertyName];
            }
            else
            {
                var anyValue = new AnyType { Type = type };
                option = new ShaderOption { Value = anyValue, Name = propertyName };
                Overrides[propertyName] = option;
            }
            return option.Value;
        }

#if UNITY_EDITOR
        private void Update()
        {
            if (!Application.isPlaying)
            {
                UpdateShaderOptions();
            }
        }
#endif
    }
}
