﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ElfGames.Extensions
{
    public interface IShaderOptionsHelper
    {
        float GetFloat(string propertyName);
        int GetInt(string propertyName);
        Color GetColor(string propertyName);
        Vector4 GetVector(string propertyName);
        Texture GetTexture(string propertyName);

        void SetFloat(string propertyName, float value);
        void SetInt(string propertyName, int value);
        void SetColor(string propertyName, Color value);
        void SetVector(string propertyName, Vector4 value);
        void SetTexture(string propertyName, Texture value);
    }

    [RequireComponent(typeof(MaskableGraphic))]
    public class UIShaderOptionsHelper : MonoBehaviour, IShaderOptionsHelper
    {
        [SerializeField] protected ShaderOption[] Options;
        private MaskableGraphic _renderer;

        readonly Dictionary<string, ShaderOption> Overrides = new Dictionary<string, ShaderOption>();

        void Awake()
        {
            _renderer = GetComponent<MaskableGraphic>();
            _renderer.material = Instantiate(_renderer.material);
            UpdateShaderOptions();
        }

        void UpdateShaderOptions()
        {
            if (_renderer == null || _renderer.material == null)
            {
                return;
            }
            foreach (var element in Options)
            {
                if (!_renderer.material.HasProperty(element.Name))
                {
                    Debug.LogErrorFormat(this, "No such property {0} in {1}", element.Name, gameObject.name);
                    continue;
                }
                element.SetPropertyValue(_renderer.material);
            }

            foreach (var element in Overrides)
            {
                if (!_renderer.material.HasProperty(element.Key))
                {
                    Debug.LogErrorFormat(this, "No such property {0} in {1}", element.Key, gameObject.name);
                    continue;
                }
                element.Value.SetPropertyValue(_renderer.material);
            }
        }

        public float GetFloat(string propertyName)
        {
            return _renderer.material.GetFloat(propertyName);
        }

        public int GetInt(string propertyName)
        {
            return _renderer.material.GetInt(propertyName);
        }

        public Color GetColor(string propertyName)
        {
            return _renderer.material.GetColor(propertyName);
        }

        public Vector4 GetVector(string propertyName)
        {
            return _renderer.material.GetVector(propertyName);
        }

        public Texture GetTexture(string propertyName)
        {
            return _renderer.material.GetTexture(propertyName);
        }

        public void SetFloat(string propertyName, float value)
        {
            LoadValue(propertyName, SupportedTypes.Float).NumericValue.x = value;
            UpdateShaderOptions();
        }

        public void SetInt(string propertyName, int value)
        {
            LoadValue(propertyName, SupportedTypes.Integer).NumericValue.x = value;
            UpdateShaderOptions();
        }

        public void SetColor(string propertyName, Color value)
        {
            LoadValue(propertyName, SupportedTypes.Color).NumericValue = value;
            UpdateShaderOptions();
        }

        public void SetVector(string propertyName, Vector4 value)
        {
            LoadValue(propertyName, SupportedTypes.Vector4).NumericValue = value;
            UpdateShaderOptions();
        }

        public void SetTexture(string propertyName, Texture value)
        {
            LoadValue(propertyName, SupportedTypes.Object).ObjectValue = value;
            UpdateShaderOptions();
        }

        AnyType LoadValue(string propertyName, SupportedTypes type)
        {
            ShaderOption option;
            if (Overrides.ContainsKey(propertyName))
            {
                option = Overrides[propertyName];
            }
            else
            {
                var anyValue = new AnyType { Type = type };
                option = new ShaderOption { Value = anyValue, Name = propertyName };
                Overrides[propertyName] = option;
            }
            return option.Value;
        }

#if UNITY_EDITOR
        private void Update()
        {
            UpdateShaderOptions();
        }
#endif
    }
}
