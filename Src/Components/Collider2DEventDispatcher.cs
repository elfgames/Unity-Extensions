using UnityEngine;
using UnityEngine.Events;

namespace ElfGames.Extensions
{
    [System.Serializable]
    public class Collider2DEvent : UnityEvent<Collider2D> { }

    public class Collider2DEventDispatcher : MonoBehaviour
    {
        [SerializeField] protected LayerMask Layers;
        [SerializeField] protected string[] Tags;
        [SerializeField] protected Collider2DEvent OnTriggerEntered2D;
        [SerializeField] protected Collider2DEvent OnTriggerExited2D;
        [SerializeField] protected Collider2DEvent OnTriggerStayed2D;

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (Layers.Contains(collision.gameObject.layer) || System.Array.IndexOf(Tags, collision.tag) != -1)
            {
                OnTriggerEntered2D.Invoke(collision);
            }
        }

        private void OnTriggerStay2D(Collider2D collision)
        {
            if (Layers.Contains(collision.gameObject.layer) || System.Array.IndexOf(Tags, collision.tag) != -1)
            {
                OnTriggerStayed2D.Invoke(collision);
            }
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            if (Layers.Contains(collision.gameObject.layer) || System.Array.IndexOf(Tags, collision.tag) != -1)
            {
                OnTriggerExited2D.Invoke(collision);
            }
        }
    }
}
