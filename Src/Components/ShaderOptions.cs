﻿using UnityEngine;

namespace ElfGames.Extensions
{
    public enum ShaderOptionsType
    {
        Integer,
        Float,
        Color,
        Vector,
        Texture
    }
    [System.Serializable]
    public class ShaderOptionsProperty
    {
        public string Name;
        public ShaderOptionsType Type;
        public string Value;
        public Texture Texture;

        public void SetPropertyValue(MaterialPropertyBlock properties)
        {
            switch (Type)
            {
                case ShaderOptionsType.Integer:
                    properties.SetInt(Name, Value.ToInteger());
                    break;
                case ShaderOptionsType.Color:
                    properties.SetColor(Name, Color.red);
                    break;
                case ShaderOptionsType.Vector:
                    properties.SetVector(Name, new Vector3().FromString(Value));
                    break;
                case ShaderOptionsType.Float:
                    properties.SetFloat(Name, Value.ToFloat());
                    break;
                case ShaderOptionsType.Texture:
                    properties.SetTexture(Name, Texture);
                    break;
            }
        }
    }

    [AddComponentMenu("")]
    [System.Obsolete("Component deprecated, please replace with ShaderOptionsHelper")]
    [RequireComponent(typeof(Renderer))]
    public class ShaderOptions : MonoBehaviour
    {
        [SerializeField] protected ShaderOptionsProperty[] Options;
        private Renderer _renderer;

        void Awake()
        {
            _renderer = GetComponent<Renderer>();
#if UNITY_EDITOR
            Debug.LogWarning("Component deprecated, please replace with ShaderOptionsHelper");
#endif
            UpdateShaderOptions();
        }

        void UpdateShaderOptions()
        {
            var properties = new MaterialPropertyBlock();
            _renderer.GetPropertyBlock(properties);

            foreach (var element in Options)
            {
                if (!_renderer.sharedMaterial.HasProperty(element.Name))
                {
                    Debug.LogErrorFormat(this, "No such property {0} in {1}", element.Name, gameObject.name);
                }
                element.SetPropertyValue(properties);
            }
            _renderer.SetPropertyBlock(properties);
        }
    }
}
