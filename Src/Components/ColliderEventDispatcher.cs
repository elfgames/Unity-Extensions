using UnityEngine;
using UnityEngine.Events;

namespace ElfGames.Extensions
{
    [System.Serializable]
    public class ColliderEvent : UnityEvent<Collider> { }

    public class ColliderEventDispatcher : MonoBehaviour
    {
        [SerializeField] protected LayerMask Layers;
        [SerializeField] protected string[] Tags;
        [SerializeField] protected ColliderEvent OnTriggerEntered;
        [SerializeField] protected ColliderEvent OnTriggerExited;
        [SerializeField] protected ColliderEvent OnTriggerStayed;

        private void OnTriggerEnter(Collider collision)
        {
            if (Layers.Contains(collision.gameObject.layer) || System.Array.IndexOf(Tags, collision.tag) != -1)
            {
                OnTriggerEntered.Invoke(collision);
            }
        }

        private void OnTriggerStay(Collider collision)
        {
            if (Layers.Contains(collision.gameObject.layer) || System.Array.IndexOf(Tags, collision.tag) != -1)
            {
                OnTriggerStayed.Invoke(collision);
            }
        }

        private void OnTriggerExit(Collider collision)
        {
            if (Layers.Contains(collision.gameObject.layer) || System.Array.IndexOf(Tags, collision.tag) != -1)
            {
                OnTriggerExited.Invoke(collision);
            }
        }
    }
}
