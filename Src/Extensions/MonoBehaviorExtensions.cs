﻿using System;
using System.Collections;
using UnityEngine;

public static class MonoBehaviorExtensions
{

    public static Coroutine Invoke(this MonoBehaviour monoBehaviour, Action action, float time)
    {
        return monoBehaviour.StartCoroutine(InvokeAfter(action, time));
    }

    public static Coroutine InvokeRepeating(this MonoBehaviour monoBehaviour, Action action, float time)
    {
        return monoBehaviour.StartCoroutine(InvokeRepeatedAfter(action, time));
    }

    static IEnumerator InvokeRepeatedAfter(Action action, float time)
    {
        while (true)
        {
            yield return new WaitForSeconds(time);
            action();
        }
    }

    static IEnumerator InvokeAfter(Action action, float time)
    {
        yield return new WaitForSeconds(time);

        action();
    }

}
