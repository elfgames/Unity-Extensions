﻿using System.Collections;
using UnityEngine;

public static class AnimatorExtensions
{
    public static bool HasParameter(this Animator animator, string paramName)
    {
        return animator.GetParameter(paramName) != null;
    }

    public static AnimatorControllerParameter GetParameter(this Animator animator, string paramName)
    {
        if (animator.runtimeAnimatorController == null)
        {
            return null;
        }
        foreach (AnimatorControllerParameter param in animator.parameters)
        {
            if (param.name == paramName)
            {
                return param;
            }
        }
        return null;
    }

    public static IEnumerator PlayAndWait(this Animator animator, string animationName, float crossFade = 0)
    {
        return PlayAndWait(animator, Animator.StringToHash(animationName), crossFade);
    }

    public static IEnumerator PlayAndWait(this Animator animator, int animationID, float crossFade = 0)
    {
        var layerId = 0;
        for (var i = 0; i < animator.layerCount; i++)
        {
            if (animator.HasState(i, animationID))
            {
                layerId = i;
                break;
            }
        }

        if (crossFade == 0)
        {
            animator.Play(animationID, layerId);
        }
        else
        {
            animator.CrossFade(animationID, crossFade, layerId);
        }

        yield return 0;

        var nextState = animator.GetNextAnimatorStateInfo(layerId);
        if (nextState.shortNameHash == animationID)
        {
            while (true)
            {
                if (nextState.shortNameHash != animationID)
                {
                    break;
                }
                yield return 0;

                nextState = animator.GetNextAnimatorStateInfo(layerId);
            }
        }

        var state = animator.GetCurrentAnimatorStateInfo(layerId);
        var time = Time.time;
        while (true)
        {
            if (state.shortNameHash != animationID)
            {
                break;
            }
            if ((Time.time - time) > state.length)
            {
                break;
            }

            yield return 0;
            state = animator.GetCurrentAnimatorStateInfo(layerId);
        }
    }

    public static IEnumerator WaitForCurrentAnimationEnd(this Animator target, int layer = 0)
    {
        var initialAnimationState = target.GetCurrentAnimatorStateInfo(layer);

        while (true)
        {
            if (target.IsAnimationEnded(initialAnimationState, layer))
            {
                break;
            }
            yield return 0;
        }
    }

    public static bool IsAnimationEnded(this Animator target, AnimatorStateInfo previousAnimationState, int layer = 0)
    {
        var animationState = target.GetCurrentAnimatorStateInfo(layer);
        return previousAnimationState.fullPathHash != animationState.fullPathHash || animationState.normalizedTime >= 1f;
    }
}
