using UnityEngine;

// <summary>
// A collection of extension methods for Vector2 and Vector3 to serialize and de-serialize them as strings.
// </summary>
public static class VectorExtensions
{
    public static Vector2 AsVector2(this string text, bool safe = false)
    {
        try
        {
            var parts = text.Split(',');
            return new Vector2(parts[0].ToFloat(), parts[1].ToFloat());
        }
        catch (System.Exception e)
        {
            if (!safe)
            {
                throw new System.Exception(string.Format("Could not parse \"{0}\" as a Vector2: {1}", text, e.Message));
            }
            return Vector2.zero;
        }
    }

    public static Vector3 AsVector3(this string text, bool safe = false)
    {
        try
        {
            var parts = text.Split(',');
            return new Vector3(parts[0].ToFloat(), parts[1].ToFloat(), parts[2].ToFloat());
        }
        catch (System.Exception e)
        {
            if (!safe)
            {
                throw new System.Exception(string.Format("Could not parse \"{0}\" as a Vector3: {1}", text, e.Message));
            }
            return Vector3.zero;
        }
    }

    public static Color AsColor(this string text, bool safe = false)
    {
        try
        {
            var parts = text.Split(',');
            return new Color(parts[0].ToFloat(), parts[1].ToFloat(), parts[2].ToFloat(), parts[3].ToFloat());
        }
        catch (System.Exception e)
        {
            if (!safe)
            {
                throw new System.Exception(string.Format("Could not parse \"{0}\" as a Vector3: {1}", text, e.Message));
            }
            return Color.black;
        }
    }

    public static Vector2 FromString(this Vector2 vector, string text)
    {
        try
        {
            var parts = text.Split(',');
            vector.x = parts[0].ToFloat();
            vector.y = parts[1].ToFloat();
            return vector;
        }
        catch (System.Exception e)
        {
            throw new System.Exception(string.Format("Could not parse \"{0}\" as a Vector2: {1}", text, e.Message));
        }
    }

    public static Vector3 FromString(this Vector3 vector, string text)
    {
        try
        {
            var parts = text.Split(',');
            vector.x = parts[0].ToFloat();
            vector.y = parts[1].ToFloat();
            vector.z = parts[2].ToFloat();
            return vector;
        }
        catch (System.Exception e)
        {
            throw new System.Exception(string.Format("Could not parse \"{0}\" as a Vector3: {1}", text, e.Message));
        }
    }

    public static int ManhattanDistance(this Vector2Int a, Vector2Int b)
    {
        return Mathf.Abs(a.x - b.x) + Mathf.Abs(a.y - b.y);
    }
    public static int ManhattanDistance(this Vector3Int a, Vector3Int b)
    {
        return Mathf.Abs(a.x - b.x) + Mathf.Abs(a.y - b.y) + Mathf.Abs(a.z - b.z);
    }
}
