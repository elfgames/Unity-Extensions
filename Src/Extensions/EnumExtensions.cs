﻿using System;
using System.Collections.Generic;
using System.Linq;
public static class EnumExtensions
{
    public static bool In(this System.Enum enumerator, params System.Enum[] enums)
    {
        foreach (var en in enums)
        {
            if (en.Equals(enumerator))
            {
                return true;
            }
        }
        return false;
    }

    public static IEnumerable<T> GetFlags<T>(this T en) where T : struct, Enum
    {
        return Enum.GetValues(typeof(T)).Cast<T>().Where(member => en.HasFlag(member));
    }
}
