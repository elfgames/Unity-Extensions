﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TransformExtensions
{
    public static string GetPath(this Transform transform)
    {
        var parent = transform.parent;
        if (parent == null)
        {
            return "/" + transform.name;
        }
        return parent.GetPath() + "/" + transform.name;
    }
}
