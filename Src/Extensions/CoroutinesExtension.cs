﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ElfGames.Extensions
{
    public static class CoroutineExtensions
    {
        public static Coroutine<T> StartCoroutine<T>(this MonoBehaviour behaviour, IEnumerator coroutine)
        {
            var coroutineObject = new Coroutine<T>();
            coroutineObject.coroutine = behaviour.StartCoroutine(coroutineObject.InternalRoutine(coroutine));
            return coroutineObject;
        }

        public static StatefulCoroutine StartStatefulCoroutine(this MonoBehaviour behaviour, IEnumerator coroutine)
        {
            var coroutineObject = new StatefulCoroutine();
            coroutineObject.coroutine = behaviour.StartCoroutine(coroutineObject.InternalRoutine(coroutine));
            return coroutineObject;
        }

        public static void StopStatefulCoroutine(this MonoBehaviour behaviour, StatefulCoroutine statefulCoroutine, bool nested = true)
        {
            if (statefulCoroutine.childRoutine != null && nested)
            {
                behaviour.StopCoroutine(statefulCoroutine.childRoutine);
            }
            if (statefulCoroutine.coroutine != null)
            {
                behaviour.StopCoroutine(statefulCoroutine.coroutine);
            }
        }
    }

    public interface IStatefulCoroutine
    {
        Exception Exception { get; }
        Coroutine coroutine { get; }
        bool isComplete { get; }
    }

    public class StatefulCoroutine : IStatefulCoroutine
    {
        public Exception Exception { get; private set; }
        public Coroutine coroutine { get; internal set; }
        public bool isComplete { get; private set; }
        public Coroutine childRoutine { get; private set; }

        public IEnumerator InternalRoutine(IEnumerator coroutine)
        {
            while (true)
            {
                try
                {
                    if (!coroutine.MoveNext())
                    {
                        isComplete = true;
                        yield break;
                    }
                }
                catch (Exception e)
                {
                    Exception = e;
                    yield break;
                }
                childRoutine = coroutine.Current as Coroutine;
                yield return coroutine.Current;
                childRoutine = null;
            }
        }
    }

    public class Coroutine<T> : IStatefulCoroutine
    {
        public T Value
        {
            get
            {
                if (Exception != null)
                {
                    throw Exception;
                }
                return returnVal;
            }
        }
        public Exception Exception { get; private set; }
        public Coroutine coroutine { get; internal set; }
        public bool isComplete { get; private set; }

        private T returnVal;

        public IEnumerator InternalRoutine(IEnumerator coroutine)
        {
            while (true)
            {
                try
                {
                    if (!coroutine.MoveNext())
                    {
                        isComplete = true;
                        yield break;
                    }
                }
                catch (Exception e)
                {
                    Exception = e;
                    yield break;
                }
                object yielded = coroutine.Current;
                if (yielded is T)
                {
                    returnVal = (T)yielded;
                    yield break;
                }
                else
                {
                    yield return coroutine.Current;
                }
            }
        }
    }

}
