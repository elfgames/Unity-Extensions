﻿using System.Threading;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

[InitializeOnLoad]
#endif
public static class StartupFixLocale
{
#if UNITY_EDITOR
	static StartupFixLocale() {
		SetLocale();
	}
#endif

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    static void SetLocale()
    {
        Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-us");
    }
}
