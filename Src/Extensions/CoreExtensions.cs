using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using JetBrains.Annotations;

public static class CoreExtensions
{
    public static T ClampAt<T>(this T[] array, int index)
    {
        if (array.Length == 0)
        {
            return default;
        }
        if (index < 0)
        {
            return array[0];
        }
        if (index >= array.Length)
        {
            return array[array.Length - 1];
        }
        return array[index];
    }

    public static T At<T>(this T[] array, int index)
    {
        if (array.Length == 0)
        {
            return default;
        }
        return array[index.DivMod(array.Length)];
    }

    public static int DivMod(this int n, int m)
    {
        return ((n % m) + m) % m;
    }

    public static int Clamp(this int n, int min, int max)
    {
        return Math.Min(max, Math.Max(n, min));
    }

    public static float DivMod(this float n, float m)
    {
        return ((n % m) + m) % m;
    }

    public static double DivMod(this double n, double m)
    {
        return ((n % m) + m) % m;
    }

    public static bool IsBlank(this string value)
    {
        return string.IsNullOrEmpty(value);
    }

    public static bool IsPresent(this string value)
    {
        return !string.IsNullOrEmpty(value);
    }

    public static string Presence(this string value)
    {
        return value.IsPresent() ? value : null;
    }

    public static int ToInteger(this string value, bool safe = false)
    {
        int i = 0;
        if (int.TryParse(value, out i))
        {
            return i;
        }
        if (safe)
        {
            return default;
        }
#if UNITY_EDITOR
		throw new Exception("Could not parse integer \"{0}\"".FormatWith(value));
#else
        UnityEngine.Debug.LogErrorFormat("Could not parse integer: {0}", value);
        return i;
#endif
    }

    public static double ToDouble(this string value, bool safe = false)
    {
        double i = 0;
        if (double.TryParse(value, NumberStyles.Float, CultureInfo.InvariantCulture, out i))
        {
            return i;
        }
        if (safe)
        {
            return default;
        }
#if UNITY_EDITOR
		throw new Exception("Could not parse double \"{0}\"".FormatWith(value));
#else
        UnityEngine.Debug.LogErrorFormat("Could not parse double: \"{0}\"", value);
        return i;
#endif
    }

    public static float ToFloat(this string value, bool safe = false)
    {
        float i = 0;
        if (float.TryParse(value, NumberStyles.Float, CultureInfo.InvariantCulture, out i))
        {
            return i;
        }
        if (safe)
        {
            return default;
        }
#if UNITY_EDITOR
		throw new Exception("Could not parse float \"{0}\"".FormatWith(value));
#else
        UnityEngine.Debug.LogErrorFormat("Could not parse float: \"{0}\"", value);
        return i;
#endif
    }

    public static Dictionary<string, T> AsDictionary<T>(this T[] array) where T : IIdentifiable
    {
        return array.ToDictionary(e => e.ID, e => e);
    }

    public static T Sample<T>(this T[] list)
    {
        return list[UnityEngine.Random.Range(0, list.Length)];
    }


    public static T Sample<T>(this IList<T> list)
    {
        return list[UnityEngine.Random.Range(0, list.Count)];
    }

    public static T Sample<T>(this IEnumerable<T> list)
    {
        var count = list.Count();
        if (count == 0)
        {
            return default;
        }
        return list.ElementAt(UnityEngine.Random.Range(0, count));
    }

    /// <summary>
    /// Estrare <param name="number"></param> elementi casuali dalla lista chiamata
    /// </summary>
    public static IList<T> ExtractRandomElements<T>(this IEnumerable<T> list, int number)
    {
        if (list.Count() <= number)
            return new List<T>(list);

        var indexSet = new HashSet<int>();
        var random = new Random();

        while (indexSet.Count < number)
            indexSet.Add(random.Next(list.Count()));

        var output = new List<T>();
        foreach (var i in indexSet)
        {
            output.Add(list.ElementAt(i));
        }

        return output;
    }

    /// <summary>
    ///     Extension method to format string with passed arguments
    /// </summary>
    /// <param name="format">string format</param>
    /// <param name="args">arguments</param>
    [StringFormatMethod("format")]
    public static string FormatWith(this string format, params object[] args)
    {
        return string.Format(format, args);
    }

    /// <summary>
    ///     "a string".IsNullOrEmpty() beats string.IsNullOrEmpty("a string")
    /// </summary>
    /// <param name="input">string in ingresso</param>
    public static bool IsNullOrEmpty(this string input)
    {
        return string.IsNullOrEmpty(input);
    }

    /// <summary>
    ///     "a string".IsNullOrWhiteSpace () beats string.IsNullOrWhiteSpace ("a string")
    /// </summary>
    /// <param name="input">string in ingresso</param>
    public static bool IsNullOrWhiteSpace(this string input)
    {
        return input == null || input.Trim().IsNullOrEmpty();
    }
}

public interface IIdentifiable
{
    string ID { get; }
}
