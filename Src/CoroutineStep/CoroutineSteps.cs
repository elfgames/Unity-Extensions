﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditorInternal;
using System.Linq;
using System.Reflection;

namespace ElfGames.Extensions
{
    [CustomPropertyDrawer(typeof(CoroutineSteps))]
    public class CoroutineStepsDrawer : PropertyDrawer
    {

        Dictionary<string, ReorderableList> lists;

        ReorderableList GetList(SerializedProperty property, string name)
        {
            if (lists == null)
            {
                lists = new Dictionary<string, ReorderableList>();
            }
            if (!lists.ContainsKey(property.propertyPath))
            {
                var list = new ReorderableList(property.serializedObject, property, true, true, true, true);
                list.elementHeight = 2 * list.elementHeight;
                list.drawHeaderCallback = (rect) => EditorGUI.LabelField(rect, name);
                list.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) => DrawElement(property, rect, index, isActive, isFocused);
                lists[property.propertyPath] = list;
            }
            return lists[property.propertyPath];
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {

            var steps = property.FindPropertyRelative("Steps");
            var list = GetList(steps, property.displayName);

            EditorGUI.BeginProperty(position, label, property);
            list.DoList(position);
            EditorGUI.EndProperty();
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            var steps = property.FindPropertyRelative("Steps");
            var list = GetList(steps, property.displayName);
            return list != null ? list.GetHeight() : base.GetPropertyHeight(property, label);
        }

        protected void DrawElement(SerializedProperty array, Rect rect, int index, bool isActive, bool isFocused)
        {
            var element = array.GetArrayElementAtIndex(index);

            var objectProperty = element.FindPropertyRelative("Object");
            var typeProperty = element.FindPropertyRelative("Type");
            var componentProperty = element.FindPropertyRelative("Component");
            var methodNameProperty = element.FindPropertyRelative("MethodName");
            var parameterProperty = element.FindPropertyRelative("Parameter");
            var asyncProperty = element.FindPropertyRelative("Async");
            EditorGUI.PropertyField(new Rect(rect.x, rect.y, rect.width / 2, EditorGUIUtility.singleLineHeight), objectProperty, GUIContent.none);

            if (objectProperty.objectReferenceValue != null)
            {
                var types = (objectProperty.objectReferenceValue as GameObject).GetComponents<MonoBehaviour>();
                var selectedComponent = DrawTypeSelector(typeProperty, componentProperty, types, rect);

                if (selectedComponent >= 0)
                {
                    var type = types[selectedComponent];
                    var validMethods = type.GetType().GetMethods(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic).Where(e => e.ReturnType == typeof(IEnumerator)).ToArray();
                    int selectedMethod = DrawMethodSelector(methodNameProperty, validMethods, rect);

                    DrawAsyncToggle(asyncProperty, rect);

                    if (selectedMethod >= 0 && validMethods.Length > selectedMethod)
                    {
                        var parameters = validMethods[selectedMethod].GetParameters();

                        DrawParameterField(parameterProperty, parameters, rect);
                    }
                }
            }
        }

        int DrawTypeSelector(SerializedProperty typeProperty, SerializedProperty componentProperty, MonoBehaviour[] types, Rect rect)
        {
            var typeNames = types.Select(e => e.GetType().FullName).ToArray();
            int selectedComponent = Array.IndexOf(typeNames, typeProperty.stringValue);

            EditorGUI.BeginChangeCheck();
            selectedComponent = EditorGUI.Popup(
                new Rect(rect.x + rect.width / 2, rect.y, rect.width / 2, EditorGUIUtility.singleLineHeight),
                selectedComponent, typeNames);

            if (EditorGUI.EndChangeCheck())
            {
                if (selectedComponent >= 0)
                {
                    typeProperty.stringValue = typeNames[selectedComponent];
                    componentProperty.objectReferenceValue = types[selectedComponent];
                    typeProperty.serializedObject.ApplyModifiedProperties();
                }
            }
            return selectedComponent;
        }

        int DrawMethodSelector(SerializedProperty methodNameProperty, MethodInfo[] validMethods, Rect rect)
        {
            var validMethodsNames = validMethods.Select(e => e.Name).ToArray();
            int selectedMethod = Array.IndexOf(validMethodsNames, methodNameProperty.stringValue);
            EditorGUI.BeginChangeCheck();

            selectedMethod = EditorGUI.Popup(
                new Rect(rect.x, rect.y + rect.height / 2, rect.width / 3, EditorGUIUtility.singleLineHeight),
                selectedMethod, validMethodsNames);

            if (EditorGUI.EndChangeCheck())
            {
                if (selectedMethod >= 0)
                {
                    methodNameProperty.stringValue = validMethodsNames[selectedMethod];
                    methodNameProperty.serializedObject.ApplyModifiedProperties();
                }
            }
            return selectedMethod;
        }

        void DrawAsyncToggle(SerializedProperty asyncProperty, Rect rect)
        {
            EditorGUI.BeginChangeCheck();
            var asyncValue = EditorGUI.ToggleLeft(
                new Rect(rect.x + rect.width / 3 + 20, rect.y + rect.height / 2, rect.width / 3, EditorGUIUtility.singleLineHeight),
                "Async",
                asyncProperty.boolValue);
            if (EditorGUI.EndChangeCheck())
            {
                asyncProperty.boolValue = asyncValue;
                asyncProperty.serializedObject.ApplyModifiedProperties();
            }
        }

        void DrawParameterField(SerializedProperty parameterProperty, ParameterInfo[] parameters, Rect rect)
        {
            if (parameters.Length == 1)
            {

                var parameterType = parameterProperty.FindPropertyRelative("Type");
                var parameter = parameters[0];

                EditorGUI.BeginChangeCheck();

                var parameterValue = parameterProperty.FindPropertyRelative("StoredValue");
                var parameterRect = new Rect(rect.x + rect.width / 3 * 2, rect.y + rect.height / 2, rect.width / 3, EditorGUIUtility.singleLineHeight);
                if (parameter.ParameterType == typeof(string))
                {
                    parameterType.enumValueIndex = (int)CoroutineSteps.ParamType.String;
                    parameterValue.stringValue = EditorGUI.TextField(parameterRect, parameterValue.stringValue);
                }
                else if (parameter.ParameterType == typeof(int))
                {
                    parameterType.enumValueIndex = (int)CoroutineSteps.ParamType.Integer;
                    parameterValue.stringValue = EditorGUI.IntField(parameterRect, parameterValue.stringValue.IsBlank() ? 0 : parameterValue.stringValue.ToInteger()).ToString();
                }
                else if (parameter.ParameterType == typeof(bool))
                {
                    parameterType.enumValueIndex = (int)CoroutineSteps.ParamType.Bool;
                    parameterValue.stringValue = EditorGUI.Toggle(parameterRect, parameterValue.stringValue == "1") ? "1" : "0";
                }
                else if (parameter.ParameterType == typeof(float))
                {
                    parameterType.enumValueIndex = (int)CoroutineSteps.ParamType.Float;
                    parameterValue.stringValue = EditorGUI.FloatField(parameterRect, parameterValue.stringValue.IsBlank() ? 0f : parameterValue.stringValue.ToFloat()).ToString();
                }

                if (EditorGUI.EndChangeCheck())
                {
                    parameterProperty.serializedObject.ApplyModifiedProperties();
                }
            }
        }
    }
}
#endif


namespace ElfGames.Extensions
{
    [Serializable]
    public class CoroutineSteps
    {

        public enum ParamType { None, Integer, Float, String, Bool }

        [Serializable]
        public class ParameterValue
        {
            public ParamType Type;
            public string StoredValue;

            public object Value
            {
                get
                {
                    switch (Type)
                    {
                        case ParamType.Float:
                            return StoredValue.ToFloat();
                        case ParamType.Integer:
                            return StoredValue.ToInteger();
                        case ParamType.Bool:
                            return StoredValue.ToInteger() > 0;
                        case ParamType.String:
                            return StoredValue;
                        default:
                            return null;
                    }
                }
            }
        }

        [Serializable]
        public class Step
        {
            public GameObject Object;
            public MonoBehaviour Component;
            public string Type;
            public string MethodName;
            public ParameterValue Parameter;
            public bool Async;
        }

        public Step[] Steps;

        public IEnumerator Invoke()
        {
            var pendingSteps = new List<Coroutine>();
            foreach (var step in Steps)
            {
                Assert.IsNotNull(step.Object, "Reference object is null");
                var monoBehavior = step.Component;
                if (monoBehavior == null)
                {
                    Debug.LogWarning("Current CoroutineSteps serialization is Obsolete. Please update!");
                    var type = Type.GetType(step.Type) ?? Type.GetType(step.Type + ", Assembly-CSharp");
                    Assert.AreNotEqual(type, null, "Invalid component type {0}".FormatWith(step.Type));
                    monoBehavior = step.Object.GetComponent(type) as MonoBehaviour;
                }

                Assert.IsNotNull(monoBehavior, "No such component {0} in {1}".FormatWith(step.Type, step.Object));

                var routine = step.Parameter.Type != ParamType.None ?
                              monoBehavior.StartCoroutine(step.MethodName, step.Parameter.Value) :
                              monoBehavior.StartCoroutine(step.MethodName);
                if (step.Async)
                {
                    pendingSteps.Add(routine);
                }
                else
                {
                    if (pendingSteps.Count > 0)
                    {
                        foreach (var r in pendingSteps)
                        {
                            yield return r;
                        }
                        pendingSteps.Clear();
                    }
                    yield return routine;
                }
            }
            if (pendingSteps.Count > 0)
            {
                foreach (var r in pendingSteps)
                {
                    yield return r;
                }
            }
        }
    }
}
