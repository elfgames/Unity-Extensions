﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using UnityEngine.Assertions;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditorInternal;

namespace ElfGames.Extensions
{
    // <summary>
    // Editor class for UnityEventSequence
    // </summary>
    [CustomPropertyDrawer(typeof(UnityEventSequence))]
    public class UnityEventSequenceDrawer : PropertyDrawer
    {
        Dictionary<string, ReorderableList> lists;

        ReorderableList GetList(SerializedProperty property, string name)
        {
            if (lists == null)
            {
                lists = new Dictionary<string, ReorderableList>();
            }
            if (!lists.ContainsKey(property.propertyPath))
            {
                var list = new ReorderableList(property.serializedObject, property, true, true, true, true);
                list.elementHeight = 2 * list.elementHeight;
                list.drawHeaderCallback = (rect) => EditorGUI.LabelField(rect, name);
                list.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) => DrawElement(property, rect, index, isActive, isFocused);
                lists[property.propertyPath] = list;
            }
            return lists[property.propertyPath];
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {

            var steps = property.FindPropertyRelative("Steps");
            var list = GetList(steps, property.displayName);

            EditorGUI.BeginProperty(position, label, property);
            list.DoList(position);
            EditorGUI.EndProperty();
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            var steps = property.FindPropertyRelative("Steps");
            var list = GetList(steps, property.displayName);
            return list != null ? list.GetHeight() : base.GetPropertyHeight(property, label);
        }

        protected void DrawElement(SerializedProperty array, Rect rect, int index, bool isActive, bool isFocused)
        {
            var element = array.GetArrayElementAtIndex(index);

            var objectProperty = element.FindPropertyRelative("Reference");
            var methodNameProperty = element.FindPropertyRelative("MethodName");
            var isCoroutineProperty = element.FindPropertyRelative("IsCoroutine");
            var parameterProperty = element.FindPropertyRelative("Parameter");
            var asyncProperty = element.FindPropertyRelative("Async");
            EditorGUI.PropertyField(new Rect(rect.x, rect.y, rect.width / 2, EditorGUIUtility.singleLineHeight), objectProperty, GUIContent.none);

            if (objectProperty.objectReferenceValue != null)
            {
                var gameObject = objectProperty.objectReferenceValue is GameObject ? (objectProperty.objectReferenceValue as GameObject) : (objectProperty.objectReferenceValue as Component).gameObject;
                var types = new List<UnityEngine.Object>();
                types.Add(gameObject);
                types.AddRange(gameObject.GetComponents<Component>());

                var selectedComponent = DrawTypeSelector(objectProperty, types.ToArray(), rect);

                if (selectedComponent >= 0)
                {
                    var type = types[selectedComponent];
                    var validMethods = type.GetType().GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic).Where(e => e.ReturnType == typeof(IEnumerator) || e.ReturnType == typeof(void)).ToArray();
                    int selectedMethod = DrawMethodSelector(methodNameProperty, isCoroutineProperty, parameterProperty, validMethods, rect);

                    DrawAsyncToggle(asyncProperty, rect);

                    if (selectedMethod >= 0 && validMethods.Length > selectedMethod)
                    {
                        var parameters = validMethods[selectedMethod].GetParameters();

                        DrawParameterField(parameterProperty, parameters, rect);
                    }
                }
            }
        }

        int DrawTypeSelector(SerializedProperty componentProperty, UnityEngine.Object[] types, Rect rect)
        {
            var typeNames = types.Select(e => e.GetType().Name).ToArray();
            int selectedComponent = Array.IndexOf(types, componentProperty.objectReferenceValue);

            EditorGUI.BeginChangeCheck();
            selectedComponent = EditorGUI.Popup(
                new Rect(rect.x + rect.width / 2, rect.y, rect.width / 2, EditorGUIUtility.singleLineHeight),
                selectedComponent, typeNames);

            if (EditorGUI.EndChangeCheck())
            {
                if (selectedComponent >= 0)
                {
                    componentProperty.objectReferenceValue = types[selectedComponent];
                    componentProperty.serializedObject.ApplyModifiedProperties();
                }
            }
            return selectedComponent;
        }

        int DrawMethodSelector(SerializedProperty methodNameProperty, SerializedProperty isCoroutineProperty, SerializedProperty parameterProperty, MethodInfo[] validMethods, Rect rect)
        {
            var validMethodsNames = validMethods.Select(e => e.Name).ToArray();
            int selectedMethod = Array.IndexOf(validMethodsNames, methodNameProperty.stringValue);
            EditorGUI.BeginChangeCheck();

            selectedMethod = EditorGUI.Popup(
                new Rect(rect.x, rect.y + rect.height / 2, rect.width / 3, EditorGUIUtility.singleLineHeight),
                selectedMethod, validMethodsNames);

            if (EditorGUI.EndChangeCheck())
            {
                if (selectedMethod >= 0)
                {
                    var parameterType = parameterProperty.FindPropertyRelative("Type");
                    parameterType.enumValueIndex = (int)CoroutineSteps.ParamType.None;
                    var parameterValue = parameterProperty.FindPropertyRelative("StoredValue");
                    parameterValue.stringValue = default;

                    isCoroutineProperty.boolValue = validMethods[selectedMethod].ReturnType != typeof(void);
                    methodNameProperty.stringValue = validMethodsNames[selectedMethod];
                    methodNameProperty.serializedObject.ApplyModifiedProperties();
                }
            }
            return selectedMethod;
        }

        void DrawAsyncToggle(SerializedProperty asyncProperty, Rect rect)
        {
            EditorGUI.BeginChangeCheck();
            var asyncValue = EditorGUI.ToggleLeft(
                new Rect(rect.x + rect.width / 3 + 20, rect.y + rect.height / 2, rect.width / 3 - 20, EditorGUIUtility.singleLineHeight),
                "Async",
                asyncProperty.boolValue);
            if (EditorGUI.EndChangeCheck())
            {
                asyncProperty.boolValue = asyncValue;
                asyncProperty.serializedObject.ApplyModifiedProperties();
            }
        }

        void DrawParameterField(SerializedProperty parameterProperty, ParameterInfo[] parameters, Rect rect)
        {
            if (parameters.Length == 1)
            {

                var parameterType = parameterProperty.FindPropertyRelative("Type");
                var parameter = parameters[0];

                EditorGUI.BeginChangeCheck();

                var parameterValue = parameterProperty.FindPropertyRelative("StoredValue");
                var parameterRect = new Rect(rect.x + rect.width / 3 * 2, rect.y + rect.height / 2, rect.width / 3, EditorGUIUtility.singleLineHeight);
                if (parameter.ParameterType == typeof(string))
                {
                    parameterType.enumValueIndex = (int)CoroutineSteps.ParamType.String;
                    parameterValue.stringValue = EditorGUI.TextField(parameterRect, parameterValue.stringValue);
                }
                else if (parameter.ParameterType == typeof(int))
                {
                    parameterType.enumValueIndex = (int)CoroutineSteps.ParamType.Integer;
                    parameterValue.stringValue = EditorGUI.IntField(parameterRect, parameterValue.stringValue.IsBlank() ? 0 : parameterValue.stringValue.ToInteger()).ToString();
                }
                else if (parameter.ParameterType == typeof(bool))
                {
                    parameterType.enumValueIndex = (int)CoroutineSteps.ParamType.Bool;
                    parameterValue.stringValue = EditorGUI.Toggle(parameterRect, parameterValue.stringValue == "1") ? "1" : "0";
                }
                else if (parameter.ParameterType == typeof(float))
                {
                    parameterType.enumValueIndex = (int)CoroutineSteps.ParamType.Float;
                    parameterValue.stringValue = EditorGUI.FloatField(parameterRect, parameterValue.stringValue.IsBlank() ? 0f : parameterValue.stringValue.ToFloat()).ToString();
                }

                if (EditorGUI.EndChangeCheck())
                {
                    parameterProperty.serializedObject.ApplyModifiedProperties();
                }
            }
        }
    }
}
#endif


namespace ElfGames.Extensions
{
    // <summary>
    // A clone of UnityEvent that works with coroutines and waits for their completion.
    // </summary>
    [Serializable]
    public class UnityEventSequence : ISerializationCallbackReceiver
    {

        public enum ParamType { None, Integer, Float, String, Bool }

        [Serializable]
        public class ParameterValue
        {
            public ParamType Type;
            public string StoredValue;

            public object Value
            {
                get
                {
                    switch (Type)
                    {
                        case ParamType.Float:
                            return StoredValue.ToFloat();
                        case ParamType.Integer:
                            return StoredValue.ToInteger();
                        case ParamType.Bool:
                            return StoredValue.ToInteger() > 0;
                        case ParamType.String:
                            return StoredValue;
                        default:
                            return null;
                    }
                }
            }
        }

        [Serializable]
        public class Step
        {
            public UnityEngine.Object Reference;
            public string MethodName;
            public bool IsCoroutine;
            public ParameterValue Parameter;
            public bool Async;
        }

        public Step[] Steps;

        Dictionary<int, MethodInfo> methodCache = new Dictionary<int, MethodInfo>();

        // <summary>
        // Invokes the sequence. It must be run with StartCoroutine.
        // </summary>
        public IEnumerator Invoke()
        {
            var pendingSteps = new List<Coroutine>();
            for (var i = 0; i < Steps.Length; i++)
            {
                var step = Steps[i];

                Assert.IsNotNull(step.Reference, "Reference object is null");

                if (step.IsCoroutine)
                {
                    var monoBehavior = step.Reference as MonoBehaviour;

                    Assert.IsNotNull(monoBehavior, "A coroutine can be called on a MonoBehaviour only, {0} given.".FormatWith(step.Reference.GetType()));

                    var routine = step.Parameter.Type != ParamType.None ?
                              monoBehavior.StartCoroutine(step.MethodName, step.Parameter.Value) :
                              monoBehavior.StartCoroutine(step.MethodName);

                    pendingSteps.Add(routine);
                }
                else
                {
                    InvokeMethod(i, step.Reference, step.MethodName, step.Parameter.Type != ParamType.None ? step.Parameter : null);
                }

                if (!step.Async)
                {
                    if (pendingSteps.Count > 0)
                    {
                        foreach (var r in pendingSteps)
                        {
                            yield return r;
                        }
                        pendingSteps.Clear();
                    }
                }
            }
            if (pendingSteps.Count > 0)
            {
                foreach (var r in pendingSteps)
                {
                    yield return r;
                }
            }
        }

        private void InvokeMethod(int index, UnityEngine.Object monoBehavior, string methodName, ParameterValue parameterValue = null)
        {
            var parameters = parameterValue != null ? new object[] { parameterValue.Value } : new object[] { };
            if (methodCache.ContainsKey(index))
            {
                methodCache[index].Invoke(monoBehavior, parameters);
            }
            else
            {
                Debug.LogErrorFormat(monoBehavior, "Method {0} not found.", methodName);
            }
        }

        public void OnBeforeSerialize()
        {
        }

        public void OnAfterDeserialize()
        {
            if (Steps == null)
            {
                return;
            }
            for (var i = 0; i < Steps.Length; i++)
            {
                var step = Steps[i];
                if (step == null || step.Reference == null)
                {
                    return;
                }

                if (!step.IsCoroutine)
                {
                    var parametersCount = step.Parameter.Type != ParamType.None ? 1 : 0;
                    var method = step.Reference.GetType()
                        .GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
                        .FirstOrDefault(e => e.Name == step.MethodName && e.ReturnType == typeof(void) && e.GetParameters().Length == parametersCount);

                    methodCache[i] = method;
                }
            }
        }
    }
}
