#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using System.Linq;

namespace ElfGames.Extensions.Editor
{
    [InitializeOnLoad]
    public static class LatestScenes
    {
        static string currentScene;
        static LatestScenes()
        {
            EditorApplication.hierarchyChanged += HierarchyWindowChanged;
        }
        static void HierarchyWindowChanged()
        {
            if (currentScene != EditorSceneManager.GetActiveScene().name)
            {
                CheckMissingReferences.FindMissingReferencesInCurrentScene();
                currentScene = EditorSceneManager.GetActiveScene().name;
            }
        }
    }

    public static class CheckMissingReferences
    {

        [MenuItem("Tools/Missing References/Show Missing Object References in all scenes", false, 51)]
        public static void MissingSpritesInAllScenes()
        {
            foreach (var scene in EditorBuildSettings.scenes.Where(s => s.enabled))
            {
                EditorSceneManager.OpenScene(scene.path);
                var objects = Object.FindObjectsOfType<GameObject>();
                FindMissingReferences(scene.path, objects);
            }
        }

        [MenuItem("Tools/Missing References/Show Missing Object References in scene", false, 50)]
        public static void FindMissingReferencesInCurrentScene()
        {
            var objects = Object.FindObjectsOfType<GameObject>();
            FindMissingReferences(EditorSceneManager.GetActiveScene().name, objects);
        }

        [MenuItem("Tools/Missing References/Show Missing Object References in assets", false, 52)]
        public static void MissingSpritesInAssets()
        {
            var allAssets = AssetDatabase.GetAllAssetPaths();
            var objs = allAssets.Select(a => AssetDatabase.LoadAssetAtPath(a, typeof(GameObject)) as GameObject).Where(a => a != null).ToArray();

            FindMissingReferences("Project", objs);
        }

        public static void FindMissingReferences(string sceneName, GameObject[] objects)
        {
            foreach (var go in objects)
            {
                var components = go.GetComponents<Component>();

                foreach (var c in components)
                {
                    if (c == null)
                    {
                        Debug.LogErrorFormat(go, "Component {0} of {1} is null", c, go.name);
                        continue;
                    }
                    var so = new SerializedObject(c);
                    var sp = so.GetIterator();

                    while (sp.NextVisible(true))
                    {
                        if (sp.propertyType == SerializedPropertyType.ObjectReference)
                        {
                            if (sp.objectReferenceValue == null && sp.objectReferenceInstanceIDValue != 0)
                            {
                                ShowError(go, sp, sceneName);
                            }
                        }
                    }
                    var animator = c as Animator;
                    if (animator != null)
                    {
                        CheckAnimatorReferences(animator);
                    }
                }
            }
        }

        public static void CheckAnimatorReferences(Animator component)
        {
            if (component.runtimeAnimatorController == null)
            {
                return;
            }
            foreach (AnimationClip ac in component.runtimeAnimatorController.animationClips)
            {
                var so = new SerializedObject(ac);
                var sp = so.GetIterator();

                while (sp.NextVisible(true))
                {
                    if (sp.propertyType == SerializedPropertyType.ObjectReference)
                    {
                        if (sp.objectReferenceValue == null && sp.objectReferenceInstanceIDValue != 0)
                        {
                            Debug.LogErrorFormat(component, "Missing reference (ID: {0}) found in: {1}, Property: {2}, Scene: {3}", sp.objectReferenceInstanceIDValue, FullObjectPath(component.gameObject), sp.propertyPath, EditorSceneManager.GetActiveScene().name);
                        }
                    }
                }
            }
        }

        static void ShowError(GameObject go, SerializedProperty property, string sceneName)
        {
            Debug.LogErrorFormat(go, "Missing reference (ID: {0}) found in: {1}, Property: {2}, Scene: {3}", property.objectReferenceInstanceIDValue, FullObjectPath(go), property.propertyPath, sceneName);
        }

        static string FullObjectPath(GameObject go)
        {
            return go.transform.parent == null ? go.name : FullObjectPath(go.transform.parent.gameObject) + "/" + go.name;
        }
    }
}
#endif
