﻿using UnityEditor;
using UnityEngine;

namespace ElfGames.Extensions.Editor
{
    public class AutoSnap : EditorWindow
    {
        private Vector3 prevPosition;
        private bool doSnap = false;
        private float snapValue = 0.32f;

        [MenuItem("Edit/Auto Snap %_l")]

        static void Init()
        {
            var window = (AutoSnap)EditorWindow.GetWindow(typeof(AutoSnap));
            window.maxSize = new Vector2(200, 100);
        }

        public void OnGUI()
        {
            doSnap = EditorGUILayout.Toggle("Auto Snap", doSnap);
            snapValue = EditorGUILayout.FloatField("Snap Value", snapValue);
            if (GUILayout.Button("Snap colliders"))
            {
                foreach (var o in Selection.gameObjects)
                {
                    SnapBoxCollider(o.GetComponent<BoxCollider2D>());
                }
            }
        }

        public void Update()
        {
            if (doSnap
                && !EditorApplication.isPlaying
                && Selection.transforms.Length > 0
                && Selection.transforms[0].position != prevPosition)
            {
                Snap();
                prevPosition = Selection.transforms[0].position;
            }
        }

        private void SnapBoxCollider(BoxCollider2D collider)
        {
            if (collider == null)
            {
                return;
            }
            collider.offset = Round((Vector2)collider.transform.position + collider.offset) - (Vector2)collider.transform.position;
            collider.size = Round((Vector2)collider.transform.position + collider.size) - (Vector2)collider.transform.position;
        }

        private void Snap()
        {
            foreach (var transform in Selection.transforms)
            {
                var t = transform.transform.position;
                t.x = Round(t.x);
                t.y = Round(t.y);
                t.z = Round(t.z);
                transform.transform.position = t;
            }
        }

        private Vector3 Round(Vector3 input)
        {
            return new Vector3(Round(input.x), Round(input.y), Round(input.z));
        }

        private Vector2 Round(Vector2 input)
        {
            return new Vector2(Round(input.x), Round(input.y));
        }

        private float Round(float input)
        {
            return snapValue * Mathf.Round((input / snapValue));
        }
    }
}
